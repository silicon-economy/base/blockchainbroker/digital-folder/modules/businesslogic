// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package TokenManager

import (
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// InitGenesis initializes the capability module's state from a provided genesis
// state.
func InitGenesis(ctx sdk.Context, k keeper.Keeper, genState types.GenesisState) {
	k.InitGenesis(ctx, genState)
}

// ExportGenesis returns the capability module's exported genesis.
func ExportGenesis(ctx sdk.Context, k keeper.Keeper) *types.GenesisState {
	genesis := types.DefaultGenesis()

	// this line is used by starport scaffolding # genesis/module/export
	// Get all tokenCopies
	tokenCopiesList := k.GetAllTokenCopies(ctx)
	for _, elem := range tokenCopiesList {
		elem := elem
		genesis.TokenCopiesList = append(genesis.TokenCopiesList, &elem)
	}

	// Get all documentTokenMapper
	documentTokenMapperList := k.GetAllDocumentTokenMapper(ctx)
	for _, elem := range documentTokenMapperList {
		elem := elem
		genesis.DocumentTokenMapperList = append(genesis.DocumentTokenMapperList, &elem)
	}

	return genesis
}
