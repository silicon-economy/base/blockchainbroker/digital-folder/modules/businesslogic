// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package cli

import (
	"fmt"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	"github.com/spf13/cobra"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// GetTxCmd returns the transaction commands for this module
func GetTxCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:                        types.ModuleName,
		Short:                      fmt.Sprintf("%s transactions subcommands", types.ModuleName),
		DisableFlagParsing:         true,
		SuggestionsMinimumDistance: 2,
		RunE:                       client.ValidateCmd,
	}

	// this line is used by starport scaffolding # 1

	cmd.AddCommand(CmdCreateToken())
	cmd.AddCommand(CmdUpdateToken())
	cmd.AddCommand(CmdActivateToken())
	cmd.AddCommand(CmdDeactivateToken())
	cmd.AddCommand(CmdMoveToken())
	cmd.AddCommand(CmdCloneToken())
	cmd.AddCommand(CmdCreateHashToken())
	cmd.AddCommand(CmdFetchTokensByWalletId())
	cmd.AddCommand(CmdFetchTokensBySegmentId())

	cmd.AddCommand(CmdFetchDocumentHash())

	cmd.AddCommand(CmdRevertToGenesis())

	return cmd
}

func CmdRevertToGenesis() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "revert-to-genesis",
		Short: "Reverts the blockchain to its genesis state",
		Args:  cobra.ExactArgs(0),
		RunE: func(cmd *cobra.Command, args []string) error {
			clientCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			msg := types.NewMsgRevertToGenesis(clientCtx.GetFromAddress().String())
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			return tx.GenerateOrBroadcastTxCLI(clientCtx, cmd.Flags(), msg)
		},
	}

	flags.AddTxFlagsToCmd(cmd)

	return cmd
}
