// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateTokenCopies{}

func NewMsgCreateTokenCopies(creator string, index string) *MsgCreateTokenCopies {
	return &MsgCreateTokenCopies{
		Creator: creator,
		Index:   index,
		Tokens:  []string{},
	}
}

func (msg *MsgCreateTokenCopies) Route() string {
	return RouterKey
}

func (msg *MsgCreateTokenCopies) Type() string {
	return "CreateTokenCopies"
}

func (msg *MsgCreateTokenCopies) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateTokenCopies) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateTokenCopies) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateTokenCopies{}

func NewMsgUpdateTokenCopies(creator string, index string) *MsgUpdateTokenCopies {
	return &MsgUpdateTokenCopies{
		Creator: creator,
		Index:   index,
	}
}

func (msg *MsgUpdateTokenCopies) Route() string {
	return RouterKey
}

func (msg *MsgUpdateTokenCopies) Type() string {
	return "UpdateTokenCopies"
}

func (msg *MsgUpdateTokenCopies) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateTokenCopies) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateTokenCopies) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgDeleteTokenCopies{}

func NewMsgDeleteTokenCopies(creator string, index string) *MsgDeleteTokenCopies {
	return &MsgDeleteTokenCopies{
		Creator: creator,
		Index:   index,
	}
}
func (msg *MsgDeleteTokenCopies) Route() string {
	return RouterKey
}

func (msg *MsgDeleteTokenCopies) Type() string {
	return "DeleteTokenCopies"
}

func (msg *MsgDeleteTokenCopies) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeleteTokenCopies) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeleteTokenCopies) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
