// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgMoveTokenToSegment{}

func NewMsgMoveTokenToSegment(creator string, tokenRefId string, sourceSegmentId string, targetSegmentId string) *MsgMoveTokenToSegment {
	return &MsgMoveTokenToSegment{
		Creator:         creator,
		TokenRefId:      tokenRefId,
		SourceSegmentId: sourceSegmentId,
		TargetSegmentId: targetSegmentId,
	}
}

func (msg *MsgMoveTokenToSegment) Route() string {
	return RouterKey
}

func (msg *MsgMoveTokenToSegment) Type() string {
	return "MoveTokenToSegment"
}

func (msg *MsgMoveTokenToSegment) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgMoveTokenToSegment) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgMoveTokenToSegment) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
