// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgFetchDocumentHash{}

func NewMsgFetchDocumentHash(creator string, id string) *MsgFetchDocumentHash {
	return &MsgFetchDocumentHash{
		Creator: creator,
		Id:      id,
	}
}

func (msg *MsgFetchDocumentHash) Route() string {
	return RouterKey
}

func (msg *MsgFetchDocumentHash) Type() string {
	return "FetchDocumentHash"
}

func (msg *MsgFetchDocumentHash) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchDocumentHash) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchDocumentHash) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgFetchDocumentHashHistory{}

func NewMsgFetchDocumentHashHistory(creator string, id string) *MsgFetchDocumentHashHistory {
	return &MsgFetchDocumentHashHistory{
		Creator: creator,
		Id:      id,
	}
}

func (msg *MsgFetchDocumentHashHistory) Route() string {
	return RouterKey
}

func (msg *MsgFetchDocumentHashHistory) Type() string {
	return "FetchDocumentHashHistory"
}

func (msg *MsgFetchDocumentHashHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchDocumentHashHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchDocumentHashHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
