// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgFetchGetWalletHistory{}

func NewMsgFetchGetWalletHistory(creator string, id string) *MsgFetchGetWalletHistory {
	return &MsgFetchGetWalletHistory{
		Creator: creator,
		Id:      id,
	}
}

func (msg *MsgFetchGetWalletHistory) Route() string {
	return RouterKey
}

func (msg *MsgFetchGetWalletHistory) Type() string {
	return "FetchGetWalletHistory"
}

func (msg *MsgFetchGetWalletHistory) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchGetWalletHistory) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchGetWalletHistory) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
