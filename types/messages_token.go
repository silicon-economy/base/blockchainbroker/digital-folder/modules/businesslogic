// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateToken{}

func NewMsgCreateToken(creator string, tokenType string, changeMessage string, segmentId string, moduleRef string) *MsgCreateToken {
	return &MsgCreateToken{
		Creator:       creator,
		TokenType:     tokenType,
		ChangeMessage: changeMessage,
		SegmentId:     segmentId,
		ModuleRef:     moduleRef,
	}
}

func (msg *MsgCreateToken) Route() string {
	return RouterKey
}

func (msg *MsgCreateToken) Type() string {
	return "CreateToken"
}

func (msg *MsgCreateToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgUpdateToken{}

func NewMsgUpdateToken(creator string, tokenRefId string, tokenType string, changeMessage string, segmentId string, moduleRef string) *MsgUpdateToken {
	return &MsgUpdateToken{
		Creator:       creator,
		TokenRefId:    tokenRefId,
		ChangeMessage: changeMessage,
		TokenType:     tokenType,
		SegmentId:     segmentId,
		ModuleRef:     moduleRef,
	}
}

func (msg *MsgUpdateToken) Route() string {
	return RouterKey
}

func (msg *MsgUpdateToken) Type() string {
	return "UpdateToken"
}

func (msg *MsgUpdateToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgUpdateToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgUpdateToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgActivateToken{}

func NewMsgActivateToken(creator string, id string, segmentId string, moduleRef string) *MsgActivateToken {
	return &MsgActivateToken{
		Creator:   creator,
		Id:        id,
		SegmentId: segmentId,
		ModuleRef: moduleRef,
	}
}

func (msg *MsgActivateToken) Route() string {
	return RouterKey
}

func (msg *MsgActivateToken) Type() string {
	return "ActivateToken"
}

func (msg *MsgActivateToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgActivateToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgActivateToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgDeactivateToken{}

func NewMsgDeactivateToken(creator string, id string) *MsgDeactivateToken {
	return &MsgDeactivateToken{
		Creator: creator,
		Id:      id,
	}
}

func (msg *MsgDeactivateToken) Route() string {
	return RouterKey
}

func (msg *MsgDeactivateToken) Type() string {
	return "DeactivateToken"
}

func (msg *MsgDeactivateToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgDeactivateToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgDeactivateToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgMoveTokenToWallet{}

func NewMsgMoveTokenToWallet(creator string, tokenRefId string, sourceSegmentId string, targetSegmentId string) *MsgMoveTokenToWallet {

	return &MsgMoveTokenToWallet{
		Creator:         creator,
		TokenRefId:      tokenRefId,
		SourceSegmentId: sourceSegmentId,
		TargetSegmentId: targetSegmentId,
	}
}

func (msg *MsgMoveTokenToWallet) Route() string {
	return RouterKey
}

func (msg *MsgMoveTokenToWallet) Type() string {
	return "MoveTokenToWallet"
}

func (msg *MsgMoveTokenToWallet) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgMoveTokenToWallet) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgMoveTokenToWallet) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}

var _ sdk.Msg = &MsgCloneToken{}

func NewMsgCloneToken(creator string, tokenId string, walletId string, moduleRef string) *MsgCloneToken {
	return &MsgCloneToken{
		Creator:   creator,
		TokenId:   tokenId,
		WalletId:  walletId,
		ModuleRef: moduleRef,
	}
}

func (msg *MsgCloneToken) Route() string {
	return RouterKey
}

func (msg *MsgCloneToken) Type() string {
	return "CloneToken"
}

func (msg *MsgCloneToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCloneToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCloneToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
