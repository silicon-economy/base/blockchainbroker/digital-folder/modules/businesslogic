// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgFetchAllTokenHistoryGlobal{}

func NewMsgFetchAllTokenHistoryGlobal(creator string) *MsgFetchAllTokenHistoryGlobal {
	return &MsgFetchAllTokenHistoryGlobal{
		Creator: creator,
	}
}

func (msg *MsgFetchAllTokenHistoryGlobal) Route() string {
	return RouterKey
}

func (msg *MsgFetchAllTokenHistoryGlobal) Type() string {
	return "FetchAllTokenHistoryGlobal"
}

func (msg *MsgFetchAllTokenHistoryGlobal) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgFetchAllTokenHistoryGlobal) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgFetchAllTokenHistoryGlobal) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
