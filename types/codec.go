// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	"github.com/cosmos/cosmos-sdk/codec"
	codecTypes "github.com/cosmos/cosmos-sdk/codec/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	// this line is used by starport scaffolding # 1
	"github.com/cosmos/cosmos-sdk/types/msgservice"
)

func RegisterCodec(cdc *codec.LegacyAmino) {
	// this line is used by starport scaffolding # 2
	cdc.RegisterConcrete(&MsgFetchAllSegmentHistory{}, "businesslogic/FetchAllSegmentHistory", nil)

	cdc.RegisterConcrete(&MsgFetchGetWallet{}, "businesslogic/FetchGetWallet", nil)

	cdc.RegisterConcrete(&MsgFetchGetSegment{}, "businesslogic/FetchGetSegment", nil)

	cdc.RegisterConcrete(&MsgFetchAllSegment{}, "businesslogic/FetchAllSegment", nil)

	cdc.RegisterConcrete(&MsgUpdateWallet{}, "businesslogic/UpdateWallet", nil)

	cdc.RegisterConcrete(&MsgCreateWallet{}, "businesslogic/CreateWallet", nil)

	cdc.RegisterConcrete(&MsgUpdateSegment{}, "businesslogic/UpdateSegment", nil)

	cdc.RegisterConcrete(&MsgFetchGetWalletHistory{}, "businesslogic/FetchGetWalletHistory", nil)

	cdc.RegisterConcrete(&MsgCreateSegment{}, "businesslogic/CreateSegment", nil)

	cdc.RegisterConcrete(&MsgFetchAllTokenHistoryGlobal{}, "businesslogic/FetchAllTokenHistoryGlobal", nil)

	cdc.RegisterConcrete(&MsgFetchGetTokenHistoryGlobal{}, "businesslogic/FetchGetTokenHistoryGlobal", nil)

	cdc.RegisterConcrete(&MsgCreateWalletWithId{}, "businesslogic/CreateWalletWithId", nil)

	cdc.RegisterConcrete(&MsgMoveTokenToSegment{}, "businesslogic/MoveTokenToSegment", nil)

	cdc.RegisterConcrete(&MsgFetchAllWalletHistory{}, "businesslogic/FetchAllWalletHistory", nil)

	cdc.RegisterConcrete(&MsgFetchAllWallet{}, "businesslogic/FetchAllWallet", nil)

	cdc.RegisterConcrete(&MsgFetchSegmentHistory{}, "businesslogic/FetchSegmentHistory", nil)

	cdc.RegisterConcrete(&MsgCreateSegmentWithId{}, "businesslogic/CreateSegmentWithId", nil)

	cdc.RegisterConcrete(&MsgCreateTokenCopies{}, "businesslogic/CreateTokenCopies", nil)
	cdc.RegisterConcrete(&MsgUpdateTokenCopies{}, "businesslogic/UpdateTokenCopies", nil)
	cdc.RegisterConcrete(&MsgDeleteTokenCopies{}, "businesslogic/DeleteTokenCopies", nil)

	cdc.RegisterConcrete(&MsgCreateDocumentTokenMapper{}, "businesslogic/CreateDocumentTokenMapper", nil)
	cdc.RegisterConcrete(&MsgUpdateDocumentTokenMapper{}, "businesslogic/UpdateDocumentTokenMapper", nil)
	cdc.RegisterConcrete(&MsgDeleteDocumentTokenMapper{}, "businesslogic/DeleteDocumentTokenMapper", nil)

	cdc.RegisterConcrete(&MsgCreateToken{}, "TokenManager/CreateToken", nil)
	cdc.RegisterConcrete(&MsgUpdateToken{}, "TokenManager/UpdateToken", nil)
	cdc.RegisterConcrete(&MsgActivateToken{}, "TokenManager/ActivateToken", nil)
	cdc.RegisterConcrete(&MsgDeactivateToken{}, "TokenManager/DeactivateToken", nil)
	cdc.RegisterConcrete(&MsgMoveTokenToWallet{}, "TokenManager/MoveTokenToWallet", nil)
	cdc.RegisterConcrete(&MsgCreateHashToken{}, "TokenManager/CreateHashToken", nil)
	cdc.RegisterConcrete(&MsgCloneToken{}, "TokenManager/CloneToken", nil)

	cdc.RegisterConcrete(&MsgFetchDocumentHash{}, "businesslogic/FetchDocumentHash", nil)
	cdc.RegisterConcrete(&MsgFetchDocumentHashHistory{}, "businesslogic/FetchDocumentHashHistory", nil)
	cdc.RegisterConcrete(&MsgFetchTokensBySegmentId{}, "businesslogic/FetchTokensBySegmentId", nil)
	cdc.RegisterConcrete(&MsgFetchTokensByWalletId{}, "businesslogic/FetchTokensByWalletId", nil)
	cdc.RegisterConcrete(&MsgRevertToGenesis{}, "businesslogic/RevertToGenesis", nil)
	cdc.RegisterConcrete(&MsgUpdateTokenInformation{}, "businesslogic/UpdateTokenInformation", nil)
	cdc.RegisterConcrete(&MsgFetchTokenHistory{}, "businesslogic/FetchTokenHistory", nil)
	cdc.RegisterConcrete(&MsgFetchToken{}, "businesslogic/FetchToken", nil)
}

func RegisterInterfaces(registry codecTypes.InterfaceRegistry) {
	// this line is used by starport scaffolding # 3
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateTokenCopies{},
		&MsgUpdateTokenCopies{},
		&MsgDeleteTokenCopies{},
	)
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateDocumentTokenMapper{},
		&MsgUpdateDocumentTokenMapper{},
		&MsgDeleteDocumentTokenMapper{},
	)

	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgCreateToken{},
		&MsgUpdateToken{},
		&MsgActivateToken{},
		&MsgDeactivateToken{},
		&MsgMoveTokenToWallet{},
		&MsgCreateHashToken{},
		&MsgCloneToken{},
		&MsgFetchDocumentHash{},
		&MsgFetchDocumentHashHistory{},
		&MsgFetchTokensBySegmentId{},
		&MsgFetchTokensByWalletId{},
		&MsgRevertToGenesis{},
		&MsgUpdateTokenInformation{},
		&MsgFetchTokenHistory{},
		&MsgFetchToken{},
	)
	msgservice.RegisterMsgServiceDesc(registry, &_Msg_serviceDesc)
}

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewAminoCodec(amino)
)
