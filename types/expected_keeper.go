// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	hashtokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
	tokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

// TokenKeeper interface to import the token keeper
type TokenKeeper interface {
	FetchAllToken(goCtx context.Context, msg *tokenTypes.MsgFetchAllToken) (*tokenTypes.MsgFetchAllTokenResponse, error)
	FetchAllTokenHistory(goCtx context.Context, msg *tokenTypes.MsgFetchAllTokenHistory) (*tokenTypes.MsgFetchAllTokenHistoryResponse, error)
	FetchToken(goCtx context.Context, msg *tokenTypes.MsgFetchToken) (*tokenTypes.MsgFetchTokenResponse, error)
	FetchTokenHistory(goCtx context.Context, msg *tokenTypes.MsgFetchTokenHistory) (*tokenTypes.MsgFetchTokenHistoryResponse, error)
	CreateToken(goCtx context.Context, msg *tokenTypes.MsgCreateToken) (*tokenTypes.MsgIdResponse, error)
	UpdateToken(goCtx context.Context, msg *tokenTypes.MsgUpdateToken) (*tokenTypes.MsgEmptyResponse, error)
	ActivateToken(goCtx context.Context, msg *tokenTypes.MsgActivateToken) (*tokenTypes.MsgEmptyResponse, error)
	DeactivateToken(goCtx context.Context, msg *tokenTypes.MsgDeactivateToken) (*tokenTypes.MsgEmptyResponse, error)
	UpdateTokenInformation(goCtx context.Context, msg *tokenTypes.MsgUpdateTokenInformation) (*tokenTypes.MsgEmptyResponse, error)
	HasToken(ctx sdk.Context, id string) bool
	SetFalse(ctx sdk.Context, key string)
	Token(c context.Context, req *tokenTypes.QueryGetTokenRequest) (*tokenTypes.QueryGetTokenResponse, error)
	RevertToGenesis(ctx sdk.Context)
}

// HashTokenKeeper interface to import the hash token keeper
type HashTokenKeeper interface {
	FetchAllToken(goCtx context.Context, msg *hashtokenTypes.MsgFetchAllToken) (*hashtokenTypes.MsgFetchAllTokenResponse, error)
	FetchAllTokenHistory(goCtx context.Context, msg *hashtokenTypes.MsgFetchAllTokenHistory) (*hashtokenTypes.MsgFetchAllTokenHistoryResponse, error)
	FetchToken(goCtx context.Context, msg *hashtokenTypes.MsgFetchToken) (*hashtokenTypes.MsgFetchTokenResponse, error)
	FetchTokenHistory(goCtx context.Context, msg *hashtokenTypes.MsgFetchTokenHistory) (*hashtokenTypes.MsgFetchTokenHistoryResponse, error)
	CreateToken(goCtx context.Context, msg *hashtokenTypes.MsgCreateToken) (*hashtokenTypes.MsgIdResponse, error)
	UpdateToken(goCtx context.Context, msg *hashtokenTypes.MsgUpdateToken) (*hashtokenTypes.MsgEmptyResponse, error)
	ActivateToken(goCtx context.Context, msg *hashtokenTypes.MsgActivateToken) (*hashtokenTypes.MsgEmptyResponse, error)
	DeactivateToken(goCtx context.Context, msg *hashtokenTypes.MsgDeactivateToken) (*hashtokenTypes.MsgEmptyResponse, error)
	UpdateTokenInformation(goCtx context.Context, msg *hashtokenTypes.MsgUpdateTokenInformation) (*hashtokenTypes.MsgEmptyResponse, error)
	HasToken(ctx sdk.Context, id string) bool
	SetFalse(ctx sdk.Context, key string)
	Token(c context.Context, req *hashtokenTypes.QueryGetTokenRequest) (*hashtokenTypes.QueryGetTokenResponse, error)
	RevertToGenesis(ctx sdk.Context)
}

// WalletKeeper interface to import the wallet keeper
type WalletKeeper interface {
	FetchTokenHistoryGlobal(goCtx context.Context, msg *tokenwalletTypes.MsgFetchGetTokenHistoryGlobal) (*tokenwalletTypes.MsgFetchGetTokenHistoryGlobalResponse, error)
	FetchTokenHistoryGlobalAll(goCtx context.Context, msg *tokenwalletTypes.MsgFetchAllTokenHistoryGlobal) (*tokenwalletTypes.MsgFetchAllTokenHistoryGlobalResponse, error)
	FetchSegmentHistory(goCtx context.Context, msg *tokenwalletTypes.MsgFetchGetSegmentHistory) (*tokenwalletTypes.MsgFetchGetSegmentHistoryResponse, error)
	FetchSegmentHistoryAll(goCtx context.Context, msg *tokenwalletTypes.MsgFetchAllSegmentHistory) (*tokenwalletTypes.MsgFetchAllSegmentHistoryResponse, error)
	FetchSegment(goCtx context.Context, msg *tokenwalletTypes.MsgFetchGetSegment) (*tokenwalletTypes.MsgFetchGetSegmentResponse, error)
	FetchSegmentAll(goCtx context.Context, msg *tokenwalletTypes.MsgFetchAllSegment) (*tokenwalletTypes.MsgFetchAllSegmentResponse, error)
	FetchWalletHistory(goCtx context.Context, msg *tokenwalletTypes.MsgFetchGetWalletHistory) (*tokenwalletTypes.MsgFetchGetWalletHistoryResponse, error)
	FetchWalletHistoryAll(goCtx context.Context, msg *tokenwalletTypes.MsgFetchAllWalletHistory) (*tokenwalletTypes.MsgFetchAllWalletHistoryResponse, error)
	FetchWallet(goCtx context.Context, msg *tokenwalletTypes.MsgFetchGetWallet) (*tokenwalletTypes.MsgFetchGetWalletResponse, error)
	FetchWalletAll(goCtx context.Context, msg *tokenwalletTypes.MsgFetchAllWallet) (*tokenwalletTypes.MsgFetchAllWalletResponse, error)
	MoveTokenToSegment(goCtx context.Context, msg *tokenwalletTypes.MsgMoveTokenToSegment) (*tokenwalletTypes.MsgEmptyResponse, error)
	MoveTokenToWallet(goCtx context.Context, msg *tokenwalletTypes.MsgMoveTokenToWallet) (*tokenwalletTypes.MsgEmptyResponse, error)
	RemoveTokenRefFromSegment(goCtx context.Context, msg *tokenwalletTypes.MsgRemoveTokenRefFromSegment) (*tokenwalletTypes.MsgEmptyResponse, error)
	CreateSegment(goCtx context.Context, msg *tokenwalletTypes.MsgCreateSegment) (*tokenwalletTypes.MsgIdResponse, error)
	CreateSegmentWithId(goCtx context.Context, msg *tokenwalletTypes.MsgCreateSegmentWithId) (*tokenwalletTypes.MsgIdResponse, error)
	UpdateSegment(goCtx context.Context, msg *tokenwalletTypes.MsgUpdateSegment) (*tokenwalletTypes.MsgEmptyResponse, error)
	CreateTokenRef(goCtx context.Context, msg *tokenwalletTypes.MsgCreateTokenRef) (*tokenwalletTypes.MsgIdResponse, error)
	CreateWallet(goCtx context.Context, msg *tokenwalletTypes.MsgCreateWallet) (*tokenwalletTypes.MsgIdResponse, error)
	CreateWalletWithId(goCtx context.Context, msg *tokenwalletTypes.MsgCreateWalletWithId) (*tokenwalletTypes.MsgIdResponse, error)
	UpdateWallet(goCtx context.Context, msg *tokenwalletTypes.MsgUpdateWallet) (*tokenwalletTypes.MsgEmptyResponse, error)
	AssignCosmosAddressToWallet(goCtx context.Context, msg *tokenwalletTypes.MsgAssignCosmosAddressToWallet) (*tokenwalletTypes.MsgEmptyResponse, error)
	HasWallet(ctx sdk.Context, id string) bool
	SetSegment(ctx sdk.Context, segment tokenwalletTypes.Segment)
	Segment(c context.Context, req *tokenwalletTypes.QueryGetSegmentRequest) (*tokenwalletTypes.QueryGetSegmentResponse, error)
	Wallet(c context.Context, req *tokenwalletTypes.QueryGetWalletRequest) (*tokenwalletTypes.QueryGetWalletResponse, error)
	RevertToGenesis(ctx sdk.Context)
}

// AuthorizationKeeper interface to import the authorization keeper
type AuthorizationKeeper interface {
	FetchAllApplicationRole(goCtx context.Context, msg *types.MsgFetchAllApplicationRole) (*types.MsgFetchAllApplicationRoleResponse, error)
	FetchAllBlockchainAccount(goCtx context.Context, msg *types.MsgFetchAllBlockchainAccount) (*types.MsgFetchAllBlockchainAccountResponse, error)
	FetchApplicationRole(goCtx context.Context, msg *types.MsgFetchApplicationRole) (*types.MsgFetchApplicationRoleResponse, error)
	FetchBlockchainAccount(goCtx context.Context, msg *types.MsgFetchBlockchainAccount) (*types.MsgFetchBlockchainAccountResponse, error)
	CreateApplicationRole(goCtx context.Context, msg *types.MsgCreateApplicationRole) (*types.MsgCreateApplicationRoleResponse, error)
	UpdateApplicationRole(goCtx context.Context, msg *types.MsgUpdateApplicationRole) (*types.MsgUpdateApplicationRoleResponse, error)
	DeactivateApplicationRole(goCtx context.Context, msg *types.MsgDeactivateApplicationRole) (*types.MsgDeactivateApplicationRoleResponse, error)
	CreateBlockchainAccount(goCtx context.Context, msg *types.MsgCreateBlockchainAccount) (*types.MsgCreateBlockchainAccountResponse, error)
	GrantAppRoleToBlockchainAccount(goCtx context.Context, msg *types.MsgGrantAppRoleToBlockchainAccount) (*types.MsgGrantAppRoleToBlockchainAccountResponse, error)
	RevokeAppRoleFromBlockchainAccount(goCtx context.Context, msg *types.MsgRevokeAppRoleFromBlockchainAccount) (*types.MsgRevokeAppRoleFromBlockchainAccountResponse, error)
	DeactivateBlockchainAccount(goCtx context.Context, msg *types.MsgDeactivateBlockchainAccount) (*types.MsgDeactivateBlockchainAccountResponse, error)
	HasRole(ctx sdk.Context, key string, applicationRole string) (bool, error)
	SetApplicationRole(ctx sdk.Context, applicationRole types.ApplicationRole)
	SetBlockchainAccount(ctx sdk.Context, blockchainAccount types.BlockchainAccount)
	RevertToGenesis(ctx sdk.Context)
}
