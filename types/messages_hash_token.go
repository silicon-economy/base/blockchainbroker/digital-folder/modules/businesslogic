// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package types

import (
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
)

var _ sdk.Msg = &MsgCreateHashToken{}

func NewMsgCreateHashToken(creator string, changeMessage string, segmentId string, document string, hash string, hashFunction string, metadata string) *MsgCreateHashToken {
	return &MsgCreateHashToken{
		Creator:       creator,
		ChangeMessage: changeMessage,
		SegmentId:     segmentId,
		Document:      document,
		Hash:          hash,
		HashFunction:  hashFunction,
		Metadata:      metadata,
	}
}

func (msg *MsgCreateHashToken) Route() string {
	return RouterKey
}

func (msg *MsgCreateHashToken) Type() string {
	return "CreateToken"
}

func (msg *MsgCreateHashToken) GetSigners() []sdk.AccAddress {
	creator, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		panic(err)
	}
	return []sdk.AccAddress{creator}
}

func (msg *MsgCreateHashToken) GetSignBytes() []byte {
	bz := ModuleCdc.MustMarshalJSON(msg)
	return sdk.MustSortJSON(bz)
}

func (msg *MsgCreateHashToken) ValidateBasic() error {
	_, err := sdk.AccAddressFromBech32(msg.Creator)
	if err != nil {
		return sdkErrors.Wrapf(sdkErrors.ErrInvalidAddress, "invalid creator address (%s)", err)
	}
	return nil
}
