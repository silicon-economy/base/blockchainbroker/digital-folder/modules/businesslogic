// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func (k msgServer) FetchTokensBySegmentId(goCtx context.Context, msg *types.MsgFetchTokensBySegmentId) (*types.QueryAllTokenResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, errRole := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.BusinessLogicGetTokensBySegmentId)
	if errRole != nil || !check {
		return &types.QueryAllTokenResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.BusinessLogicGetTokensBySegmentId)
	}

	res, errTokens := k.TokensBySegmentId(goCtx, &types.QueryAllTokenByIdRequest{
		Id: msg.Id,
	})
	if errTokens != nil {
		return &types.QueryAllTokenResponse{}, errTokens
	}

	return &types.QueryAllTokenResponse{Token: res.Token, HashTokens: res.HashTokens}, nil
}
