// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func TestDocumentTokenMapperMsgServerCreate(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	srv := NewMsgServerImpl(*keeper)
	wctx := sdk.WrapSDKContext(ctx)
	creator := "A"
	for i := 0; i < 5; i++ {
		idx := fmt.Sprintf("%d", i)
		expected := &types.MsgCreateDocumentTokenMapper{Creator: creator, DocumentId: idx}
		_, err := srv.CreateDocumentTokenMapper(wctx, expected)
		require.NoError(t, err)
		rst, found := keeper.GetDocumentTokenMapper(ctx, expected.DocumentId)
		require.True(t, found)
		assert.Equal(t, expected.Creator, rst.Creator)
	}
}

func TestDocumentTokenMapperMsgServerUpdate(t *testing.T) {
	creator := "A"
	index := "any"

	for _, tc := range []struct {
		desc    string
		request *types.MsgUpdateDocumentTokenMapper
		err     error
	}{
		{
			desc:    "Completed",
			request: &types.MsgUpdateDocumentTokenMapper{Creator: creator, DocumentId: index},
		},
		{
			desc:    "Unauthorized",
			request: &types.MsgUpdateDocumentTokenMapper{Creator: "B", DocumentId: index},
			err:     sdkErrors.ErrUnauthorized,
		},
		{
			desc:    "KeyNotFound",
			request: &types.MsgUpdateDocumentTokenMapper{Creator: creator, DocumentId: "missing"},
			err:     sdkErrors.ErrKeyNotFound,
		},
	} {
		tc := tc
		t.Run(tc.desc, func(t *testing.T) {
			keeper, ctx := setupKeeper(t)
			srv := NewMsgServerImpl(*keeper)
			wctx := sdk.WrapSDKContext(ctx)
			expected := &types.MsgCreateDocumentTokenMapper{Creator: creator, DocumentId: index}
			_, err := srv.CreateDocumentTokenMapper(wctx, expected)
			require.NoError(t, err)

			_, err = srv.UpdateDocumentTokenMapper(wctx, tc.request)
			if tc.err != nil {
				require.ErrorIs(t, err, tc.err)
			} else {
				require.NoError(t, err)
				rst, found := keeper.GetDocumentTokenMapper(ctx, expected.DocumentId)
				require.True(t, found)
				assert.Equal(t, expected.Creator, rst.Creator)
			}
		})
	}
}
