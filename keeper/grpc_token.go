// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	hashtokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
	tokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k Keeper) TokensBySegmentId(c context.Context, req *types.QueryAllTokenByIdRequest) (*types.QueryAllTokenResponse, error) {
	// Keepers
	walletKeeper := k.walletKeeper
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper

	segment, err := walletKeeper.Segment(c, &tokenwalletTypes.QueryGetSegmentRequest{Id: req.Id})
	if err != nil {
		return &types.QueryAllTokenResponse{}, err
	}

	// iterate through segment tokenRefs and get specific tokens
	var tokens []*tokenTypes.Token
	var hashTokens []*hashtokenTypes.Token
	for i := 0; i < len(segment.Segment.TokenRefs); i++ {
		switch segment.Segment.TokenRefs[i].ModuleRef {
		case tokenTypes.ModuleName:
			token, _ := tokenKeeper.Token(c, &tokenTypes.QueryGetTokenRequest{Id: segment.Segment.WalletId + "/" + segment.Segment.TokenRefs[i].Id})
			tokens = append(tokens, token.Token)
		case hashtokenTypes.ModuleName:
			token, _ := hashTokenKeeper.Token(c, &hashtokenTypes.QueryGetTokenRequest{Id: segment.Segment.WalletId + "/" + segment.Segment.TokenRefs[i].Id})
			hashTokens = append(hashTokens, token.Token)
		}
	}

	return &types.QueryAllTokenResponse{Token: tokens, HashTokens: hashTokens}, nil
}

func (k Keeper) TokensByWalletId(c context.Context, req *types.QueryAllTokenByIdRequest) (*types.QueryAllTokenResponse, error) {
	// Keepers
	walletKeeper := k.walletKeeper
	walletResponse, err := walletKeeper.Wallet(c, &tokenwalletTypes.QueryGetWalletRequest{Id: req.Id})

	// if wallet does not exist
	if err != nil {
		return &types.QueryAllTokenResponse{}, err
	}

	// iterate through all segments in wallet and get TokensBySegmentId
	tokens := []*tokenTypes.Token{}
	hashTokens := []*hashtokenTypes.Token{}
	for i := 0; i < len(walletResponse.Wallet.SegmentIds); i++ {
		tokensBySegmentIdResponse, _ := k.TokensBySegmentId(c, &types.QueryAllTokenByIdRequest{Id: walletResponse.Wallet.SegmentIds[i]})
		tokens = append(tokens, tokensBySegmentIdResponse.Token...)
		hashTokens = append(hashTokens, tokensBySegmentIdResponse.HashTokens...)
	}

	return &types.QueryAllTokenResponse{Token: tokens, HashTokens: hashTokens}, nil
}
