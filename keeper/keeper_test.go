// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"strings"
	"testing"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	hashtokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
	tokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"
)

func TestCreateToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}
	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper
	setup := []types.MsgCreateToken{
		{
			Creator:       creatorA,
			TokenType:     tokenType1,
			ChangeMessage: notChanged,
			SegmentId:     segmentId1,
			ModuleRef:     token,
		},
		{
			Creator:       creatorA,
			TokenType:     tokenType1,
			ChangeMessage: notChanged,
			SegmentId:     segmentId1,
			ModuleRef:     hashToken,
		},
	}

	for _, element := range setup {
		newAssert := assert.New(t)
		id, _ := k.CreateToken(goCtx, &element)
		tokenRefId := strings.Split(id.Id, "/")[1]
		moduleRef, _ := k.getModuleRef(ctx, "0", tokenRefId, creatorA)
		if element.ModuleRef == token {
			outRes, _ := tokenKeeper.FetchToken(goCtx, tokenTypes.NewMsgFetchToken(creatorA, id.Id))
			out := outRes.Token
			newAssert.Equal(creatorA, out.Creator)
			newAssert.Equal(tokenType1, out.TokenType)
			newAssert.Equal(notChanged, out.ChangeMessage)
			newAssert.Equal(segmentId1, out.SegmentId)
			newAssert.True(out.Valid)
			newAssert.NotNil(out.Timestamp)
			newAssert.Equal(element.ModuleRef, moduleRef)
		} else {
			outRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokenTypes.NewMsgFetchToken(creatorA, id.Id))
			out := outRes.Token
			newAssert.Equal(creatorA, out.Creator)
			newAssert.Equal(tokenType1, out.TokenType)
			newAssert.Equal(notChanged, out.ChangeMessage)
			newAssert.Equal(segmentId1, out.SegmentId)
			newAssert.True(out.Valid)
			newAssert.NotNil(out.Timestamp)
			newAssert.Equal(element.ModuleRef, moduleRef)
		}
	}
}

func TestUpdateToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}

	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper
	tokenId, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
		Creator:       creatorA,
		TokenType:     tokenType1,
		ChangeMessage: notChanged,
		SegmentId:     segmentId1,
		ModuleRef:     token,
	})
	hashTokenId, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
		Creator:       creatorA,
		TokenType:     tokenType1,
		ChangeMessage: notChanged,
		SegmentId:     segmentId1,
		ModuleRef:     hashToken,
	})
	setup := []types.MsgUpdateToken{
		{
			Creator:       creatorA,
			TokenRefId:    strings.Split(tokenId.Id, "/")[1],
			TokenType:     tokenType2,
			ChangeMessage: changed,
			SegmentId:     segmentId1,
			ModuleRef:     token,
		},
		{
			Creator:       creatorA,
			TokenRefId:    strings.Split(hashTokenId.Id, "/")[1],
			TokenType:     tokenType2,
			ChangeMessage: changed,
			SegmentId:     segmentId1,
			ModuleRef:     hashToken,
		},
	}

	for _, element := range setup {
		newAssert := assert.New(t)
		k.UpdateToken(goCtx, &element)

		if element.ModuleRef == token {
			outRes, _ := tokenKeeper.FetchToken(goCtx, tokenTypes.NewMsgFetchToken(creatorA, "0/"+element.TokenRefId))
			out := outRes.Token
			newAssert.Equal(creatorA, out.Creator)
			newAssert.Equal(tokenType2, out.TokenType)
			newAssert.Equal(changed, out.ChangeMessage)
			newAssert.True(out.Valid)
			newAssert.NotNil(out.Timestamp)
		} else {
			outRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokenTypes.NewMsgFetchToken(creatorA, "0/"+element.TokenRefId))
			out := outRes.Token
			newAssert.Equal(creatorA, out.Creator)
			newAssert.Equal(tokenType2, out.TokenType)
			newAssert.Equal(changed, out.ChangeMessage)
			newAssert.True(out.Valid)
			newAssert.NotNil(out.Timestamp)
		}
	}
}

func TestDeactivateToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}
	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper
	tokenId, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
		Creator:       creatorA,
		TokenType:     tokenType1,
		ChangeMessage: notChanged,
		SegmentId:     segmentId1,
		ModuleRef:     token,
	})
	hashTokenId, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
		Creator:       creatorA,
		TokenType:     tokenType1,
		ChangeMessage: notChanged,
		SegmentId:     segmentId1,
		ModuleRef:     hashToken,
	})
	setup := []types.MsgDeactivateToken{
		{
			Creator: creatorA,
			Id:      tokenId.Id,
		},
		{
			Creator: creatorA,
			Id:      hashTokenId.Id,
		},
	}

	for index, element := range setup {
		newAssert := assert.New(t)
		k.DeactivateToken(goCtx, &element)

		if index == 0 {
			outRes, _ := tokenKeeper.FetchToken(goCtx, tokenTypes.NewMsgFetchToken(creatorA, tokenId.Id))
			out := outRes.Token
			newAssert.False(out.Valid)
		} else {
			outRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokenTypes.NewMsgFetchToken(creatorA, tokenId.Id))
			out := outRes.Token
			newAssert.False(out.Valid)
		}
	}
}

func TestActivateToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}
	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper
	tokenId, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
		Creator:       creatorA,
		TokenType:     tokenType1,
		ChangeMessage: notChanged,
		SegmentId:     segmentId1,
		ModuleRef:     token,
	})
	hashTokenId, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
		Creator:       creatorA,
		TokenType:     tokenType1,
		ChangeMessage: notChanged,
		SegmentId:     segmentId1,
		ModuleRef:     hashToken,
	})
	setup := []types.MsgActivateToken{
		{
			Creator:   creatorA,
			Id:        tokenId.Id,
			SegmentId: segmentId1,
			ModuleRef: token,
		},
		{
			Creator:   creatorA,
			Id:        hashTokenId.Id,
			SegmentId: segmentId1,
			ModuleRef: hashToken,
		},
	}

	for index, element := range setup {
		newAssert := assert.New(t)

		if index == 0 {
			k.DeactivateToken(goCtx, &types.MsgDeactivateToken{
				Creator: creatorA,
				Id:      tokenId.Id,
			})
			outRes, _ := tokenKeeper.FetchToken(goCtx, tokenTypes.NewMsgFetchToken(creatorA, tokenId.Id))
			out := outRes.Token
			newAssert.False(out.Valid)
			k.ActivateToken(goCtx, &element)
			outRes, _ = tokenKeeper.FetchToken(goCtx, tokenTypes.NewMsgFetchToken(creatorA, tokenId.Id))
			out = outRes.Token
			newAssert.True(out.Valid)
		} else {
			k.DeactivateToken(goCtx, &types.MsgDeactivateToken{
				Creator: creatorA,
				Id:      hashTokenId.Id,
			})
			outRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokenTypes.NewMsgFetchToken(creatorA, tokenId.Id))
			out := outRes.Token
			newAssert.False(out.Valid)
			k.ActivateToken(goCtx, &element)
			outRes, _ = hashTokenKeeper.FetchToken(goCtx, hashtokenTypes.NewMsgFetchToken(creatorA, tokenId.Id))
			out = outRes.Token
			_ = out
		}
	}
}

func TestMoveTokenToWallet(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}
	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet-01"))
	walletKeeper.CreateWallet(goCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet-02"))
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper
	tokenId, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
		Creator:       creatorA,
		TokenType:     tokenType1,
		ChangeMessage: notChanged,
		SegmentId:     segmentId1,
		ModuleRef:     token,
	})
	hashTokenId, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
		Creator:       creatorA,
		TokenType:     tokenType1,
		ChangeMessage: notChanged,
		SegmentId:     segmentId1,
		ModuleRef:     hashToken,
	})
	setup := []types.MsgMoveTokenToWallet{
		{
			Creator:         creatorA,
			TokenRefId:      strings.Split(tokenId.Id, "/")[1],
			SourceSegmentId: segmentId1,
			TargetSegmentId: segmentId2,
		},
		{
			Creator:         creatorA,
			TokenRefId:      strings.Split(hashTokenId.Id, "/")[1],
			SourceSegmentId: segmentId1,
			TargetSegmentId: segmentId2,
		},
	}

	for index, element := range setup {
		newAssert := assert.New(t)

		if index == 0 {
			k.MoveTokenToWallet(goCtx, &element)
			outRes, _ := tokenKeeper.FetchToken(goCtx, tokenTypes.NewMsgFetchToken(creatorA, "1/"+strings.Split(tokenId.Id, "/")[1]))
			out := outRes.Token
			newAssert.Equal("1", out.SegmentId)

		} else {
			k.MoveTokenToWallet(goCtx, &element)
			outRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokenTypes.NewMsgFetchToken(creatorA, "1/"+strings.Split(tokenId.Id, "/")[1]))
			out := outRes.Token
			_ = out
		}
	}
}

func TestCloneToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}
	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet-01"))
	walletKeeper.CreateWallet(goCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet-02"))
	tokenKeeper := k.tokenKeeper
	hashTokenKeeper := k.hashtokenKeeper
	tokenId, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
		Creator:       creatorA,
		TokenType:     tokenType1,
		ChangeMessage: notChanged,
		SegmentId:     segmentId1,
		ModuleRef:     token,
	})
	hashTokenId, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
		Creator:       creatorA,
		TokenType:     tokenType1,
		ChangeMessage: notChanged,
		SegmentId:     segmentId1,
		ModuleRef:     hashToken,
	})
	setup := []types.MsgCloneToken{
		{
			Creator:   creatorA,
			TokenId:   tokenId.Id,
			WalletId:  "1",
			ModuleRef: token,
		},
		{
			Creator:   creatorA,
			TokenId:   hashTokenId.Id,
			WalletId:  "1",
			ModuleRef: hashToken,
		},
	}

	for index, element := range setup {
		newAssert := assert.New(t)

		if index == 0 {
			tokenRefId := strings.Split(tokenId.Id, "/")[1]
			k.CloneToken(goCtx, &element)
			outRes, _ := tokenKeeper.FetchToken(goCtx, tokenTypes.NewMsgFetchToken(creatorA, "1/"+tokenRefId))
			out := outRes.Token
			newAssert.Equal("1", out.SegmentId)
			tokenCps, found := k.GetTokenCopies(ctx, tokenRefId)
			newAssert.True(found)
			newAssert.Equal(len(tokenCps.Tokens), 2)
		} else {
			tokenRefId := strings.Split(hashTokenId.Id, "/")[1]
			k.CloneToken(goCtx, &element)
			outRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokenTypes.NewMsgFetchToken(creatorA, "1/"+tokenRefId))
			out := outRes.Token
			newAssert.Equal("1", out.SegmentId)
			tokenCps, found := k.GetTokenCopies(ctx, tokenRefId)
			newAssert.True(found)
			newAssert.Equal(len(tokenCps.Tokens), 2)
		}
	}
}

func TestStoreDocumentHash(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	k := msgServer{
		Keeper: *newKeeper,
	}
	walletKeeper := k.walletKeeper
	walletKeeper.CreateWallet(goCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))
	setup := []types.MsgCreateHashToken{
		{
			Creator:       creatorA,
			ChangeMessage: notChanged,
			SegmentId:     segmentId1,
			Document:      document,
			Hash:          hash,
			HashFunction:  hashFunction,
			Metadata:      metadata,
		},
	}

	for _, element := range setup {
		newAssert := assert.New(t)
		id, _ := k.StoreDocumentHash(goCtx, &element)
		out, _ := k.FetchDocumentHash(goCtx, &types.MsgFetchDocumentHash{Creator: creatorA, Id: id.Id})
		newAssert.Equal(creatorA, out.Creator)
		newAssert.Equal(hash, out.Hash)
		newAssert.Equal(hashFunction, out.HashFunction)
		newAssert.Equal(metadata, out.Metadata)
		newAssert.NotNil(out.Timestamp)
	}
}

func TestLogger(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	k := msgServer{
		Keeper: *newKeeper,
	}
	logger := k.Logger(ctx)
	assert.NotNil(t, logger)
}
