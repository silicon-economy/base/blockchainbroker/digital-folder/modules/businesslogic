// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// SetTokenCopies set a specific tokenCopies in the store from its index
func (k Keeper) SetTokenCopies(ctx sdk.Context, tokenCopies types.TokenCopies) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenCopiesKey))
	b := k.cdc.MustMarshal(&tokenCopies)
	store.Set(types.KeyPrefix(tokenCopies.Index), b)
}

// GetTokenCopies returns a tokenCopies from its index
func (k Keeper) GetTokenCopies(ctx sdk.Context, index string) (val types.TokenCopies, found bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenCopiesKey))

	b := store.Get(types.KeyPrefix(index))
	if b == nil {
		return val, false
	}

	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// GetAllTokenCopies returns all tokenCopies
func (k Keeper) GetAllTokenCopies(ctx sdk.Context) (list []types.TokenCopies) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenCopiesKey))
	iterator := sdk.KVStorePrefixIterator(store, []byte{})

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var val types.TokenCopies
		k.cdc.MustUnmarshal(iterator.Value(), &val)
		list = append(list, val)
	}

	return
}
