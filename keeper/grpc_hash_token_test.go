package keeper

import (
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestFetchDocumentHashByIdFound(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	wCtx := sdk.WrapSDKContext(ctx)
	srv := NewMsgServerImpl(*keeper)

	keeper.walletKeeper.CreateWallet(wCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName))

	responseFromCreation, errorFromCreation := srv.StoreDocumentHash(wCtx, &types.MsgCreateHashToken{
		Creator:       creatorA,
		ChangeMessage: changed,
		SegmentId:     segmentId1,
		Document:      document,
		Hash:          hash,
		HashFunction:  hashFunction,
		Metadata:      metadata,
	})
	require.NoError(t, errorFromCreation)

	t.Run("Fetch DocumentHash By Id Found", func(t *testing.T) {
		response, err := srv.FetchDocumentHash(
			wCtx,
			types.NewMsgFetchDocumentHash(creatorA, responseFromCreation.Id),
		)
		assert.NoError(t, err)
		assert.NotEmpty(t, response)
		assert.Equal(t, response.Hash, hash)
	})
}

func TestFetchDocumentHashByIdNotFound(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	wCtx := sdk.WrapSDKContext(ctx)
	srv := NewMsgServerImpl(*keeper)

	t.Run("Fetch DocumentHash ById Not Found", func(t *testing.T) {
		response, err := srv.FetchDocumentHash(
			wCtx,
			types.NewMsgFetchDocumentHash(creatorA, "123"),
		)
		assert.Error(t, err)
		assert.Empty(t, response)
	})
}

func TestFetchDocumentHashHistoryByIdWithOneEntry(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	wCtx := sdk.WrapSDKContext(ctx)
	srv := NewMsgServerImpl(*keeper)

	keeper.walletKeeper.CreateWallet(
		wCtx,
		tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName),
	)

	responseFromCreation, errorFromCreation := srv.StoreDocumentHash(wCtx, &types.MsgCreateHashToken{
		Creator:       creatorA,
		ChangeMessage: changed,
		SegmentId:     segmentId1,
		Document:      document,
		Hash:          hash,
		HashFunction:  hashFunction,
		Metadata:      metadata,
	})
	require.NoError(t, errorFromCreation)

	t.Run("Fetch DocumentHashHistory By Id With One Entry", func(t *testing.T) {
		response, err := srv.FetchDocumentHashHistory(
			wCtx,
			types.NewMsgFetchDocumentHashHistory(creatorA, responseFromCreation.Id),
		)
		assert.NoError(t, err)
		assert.NotEmpty(t, response)
		assert.NotEmpty(t, response.History)
		assert.Len(t, response.History, 1)
	})
}

func TestFetchDocumentHashHistoryByIdWithTwoEntries(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	wCtx := sdk.WrapSDKContext(ctx)
	srv := NewMsgServerImpl(*keeper)

	keeper.walletKeeper.CreateWallet(
		wCtx,
		tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName),
	)

	var responseFromCreation *types.MsgIdResponse
	var errorFromCreation error

	for i := 0; i < 2; i++ {
		responseFromCreation, errorFromCreation = srv.StoreDocumentHash(wCtx, &types.MsgCreateHashToken{
			Creator:       creatorA,
			ChangeMessage: changed,
			SegmentId:     segmentId1,
			Document:      document,
			Hash:          hash,
			HashFunction:  hashFunction,
			Metadata:      metadata,
		})
		require.NoError(t, errorFromCreation)
	}

	t.Run("Fetch DocumentHashHistory By Id With Two Entries", func(t *testing.T) {
		response, err := srv.FetchDocumentHashHistory(
			wCtx,
			types.NewMsgFetchDocumentHashHistory(creatorA, responseFromCreation.Id),
		)
		assert.NoError(t, err)
		assert.NotEmpty(t, response)
		assert.NotEmpty(t, response.History)
		assert.Len(t, response.History, 2)
	})
}

func TestFetchDocumentHashHistoryByIdNotFound(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	wCtx := sdk.WrapSDKContext(ctx)
	srv := NewMsgServerImpl(*keeper)

	t.Run("Fetch DocumentHashHistory By Id Not Found", func(t *testing.T) {
		response, err := srv.FetchDocumentHashHistory(
			wCtx,
			types.NewMsgFetchDocumentHashHistory(creatorA, "456"),
		)
		assert.Error(t, err)
		assert.Empty(t, response)
	})
}
