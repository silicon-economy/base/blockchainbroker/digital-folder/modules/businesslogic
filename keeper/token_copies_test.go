// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func createNTokenCopies(keeper *Keeper, ctx sdk.Context, n int) []types.TokenCopies {
	items := make([]types.TokenCopies, n)
	for i := range items {
		items[i].Creator = "any"
		items[i].Index = fmt.Sprintf("%d", i)
		keeper.SetTokenCopies(ctx, items[i])
	}
	return items
}

func TestTokenCopiesGet(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNTokenCopies(keeper, ctx, 10)
	for _, item := range items {
		rst, found := keeper.GetTokenCopies(ctx, item.Index)
		assert.True(t, found)
		assert.Equal(t, item, rst)
	}
}

func TestTokenCopiesGetAll(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	items := createNTokenCopies(keeper, ctx, 10)
	assert.Equal(t, items, keeper.GetAllTokenCopies(ctx))
}
