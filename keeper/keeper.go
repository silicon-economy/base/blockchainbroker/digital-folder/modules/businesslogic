// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"

	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/tendermint/tendermint/libs/log"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

type (
	Keeper struct {
		cdc                 codec.Codec
		storeKey            sdk.StoreKey
		walletKeeper        types.WalletKeeper
		tokenKeeper         types.TokenKeeper
		hashtokenKeeper     types.HashTokenKeeper
		authorizationKeeper types.AuthorizationKeeper
		memKey              sdk.StoreKey
	}
)

func NewKeeper(
	cdc codec.Codec,
	storeKey sdk.StoreKey,
	walletKeeper types.WalletKeeper,
	tokenKeeper types.TokenKeeper,
	hashtokenKeeper types.HashTokenKeeper,
	authorizationKeeper types.AuthorizationKeeper,
	memKey sdk.StoreKey) *Keeper {
	return &Keeper{
		cdc:                 cdc,
		storeKey:            storeKey,
		walletKeeper:        walletKeeper,
		tokenKeeper:         tokenKeeper,
		hashtokenKeeper:     hashtokenKeeper,
		authorizationKeeper: authorizationKeeper,
		memKey:              memKey,
	}
}

func (k Keeper) Logger(ctx sdk.Context) log.Logger {
	return ctx.Logger().With("module", fmt.Sprintf("x/%s", types.ModuleName))
}

// InitGenesis initializes the capability module's state from a provided genesis state.
func (k Keeper) InitGenesis(ctx sdk.Context, genState types.GenesisState) {
	// this line is used by starport scaffolding # genesis/module/init
	// Set all the tokenCopies
	for _, elem := range genState.TokenCopiesList {
		k.SetTokenCopies(ctx, *elem)
	}

	// Set all the documentTokenMapper
	for _, elem := range genState.DocumentTokenMapperList {
		k.SetDocumentTokenMapper(ctx, *elem)
	}
}

// RevertToGenesis reverts to genesis state
func (k Keeper) RevertToGenesis(ctx sdk.Context) {
	store := ctx.KVStore(k.storeKey)
	it := store.Iterator(nil, nil)
	defer it.Close()

	for ; it.Valid(); it.Next() {
		store.Delete(it.Key())
	}

	k.InitGenesis(ctx, *types.DefaultGenesis())
}
