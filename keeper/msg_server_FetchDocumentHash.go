// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func (k msgServer) FetchDocumentHash(goCtx context.Context, msg *types.MsgFetchDocumentHash) (*types.QueryGetDocumentHashResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	authorizationKeeper := k.authorizationKeeper
	check, errRole := authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.BusinessLogicGetDocumentHash)
	if errRole != nil || !check {
		return &types.QueryGetDocumentHashResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.BusinessLogicGetDocumentHash)
	}

	res, errDocumentHash := k.DocumentHash(goCtx, &types.QueryGetDocumentHashRequest{
		Id: msg.Id,
	})
	if errDocumentHash != nil {
		return &types.QueryGetDocumentHashResponse{}, errDocumentHash
	}

	return res, nil
}

func (k msgServer) FetchDocumentHashHistory(goCtx context.Context, msg *types.MsgFetchDocumentHashHistory) (*types.QueryGetDocumentHashHistoryResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	authorizationKeeper := k.authorizationKeeper
	check, errRole := authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.BusinessLogicGetDocumentHash)
	if errRole != nil || !check {
		return &types.QueryGetDocumentHashHistoryResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.BusinessLogicGetDocumentHash)
	}

	res, errDocumentHash := k.DocumentHashHistory(goCtx, &types.QueryGetDocumentHashHistoryRequest{
		Id: msg.Id,
	})
	if errDocumentHash != nil {
		return &types.QueryGetDocumentHashHistoryResponse{}, errDocumentHash
	}

	return res, nil
}
