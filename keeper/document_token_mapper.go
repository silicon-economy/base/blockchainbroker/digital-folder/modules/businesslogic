// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

// SetDocumentTokenMapper sets a specific documentTokenMapper in the store from its index
func (k Keeper) SetDocumentTokenMapper(ctx sdk.Context, documentTokenMapper types.DocumentTokenMapper) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DocumentTokenMapperKey))
	b := k.cdc.MustMarshal(&documentTokenMapper)
	store.Set(types.KeyPrefix(documentTokenMapper.DocumentId), b)
}

// GetDocumentTokenMapper returns a documentTokenMapper from its index
func (k Keeper) GetDocumentTokenMapper(ctx sdk.Context, index string) (val types.DocumentTokenMapper, found bool) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DocumentTokenMapperKey))
	b := store.Get(types.KeyPrefix(index))
	if b == nil {
		return val, false
	}
	k.cdc.MustUnmarshal(b, &val)
	return val, true
}

// GetAllDocumentTokenMapper returns all documentTokenMapper
func (k Keeper) GetAllDocumentTokenMapper(ctx sdk.Context) (list []types.DocumentTokenMapper) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.DocumentTokenMapperKey))
	iterator := sdk.KVStorePrefixIterator(store, []byte{})

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var val types.DocumentTokenMapper
		k.cdc.MustUnmarshal(iterator.Value(), &val)
		list = append(list, val)
	}

	return
}
