// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func Test_msgServer_MsgFetchToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchToken
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchTokenResponse
		wantErr bool
	}{
		{
			name:   permissionDenied,
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgFetchToken{
				Creator: creatorA,
				Id:      "0",
			}},
			want:    &types.MsgFetchTokenResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}
			_, err := k.FetchToken(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.MsgFetchToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
