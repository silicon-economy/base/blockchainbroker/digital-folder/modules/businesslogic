// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func Test_msgServer_storeDocumentHash(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgCreateHashToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgIdResponse
		wantErr bool
	}{
		{
			name:   successfulTx,
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgCreateHashToken{
				Creator:       creatorA,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				Document:      "document-01",
				Hash:          hash,
				HashFunction:  hashFunction,
				Metadata:      metadata,
			}},
			wantErr: false,
		},
		{
			name:   permissionDenied,
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgCreateHashToken{
				Creator:       creatorB,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				Document:      "document-01",
				Hash:          hash,
				HashFunction:  hashFunction,
				Metadata:      metadata,
			}},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}
			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))
			authorizationKeeper := k.authorizationKeeper
			authorizationKeeper.SetApplicationRole(ctx, authorizationTypes.ApplicationRole{
				Creator: creatorA,
				Id:      authorizationTypes.BusinessLogicStoreDocumentHash,
				ApplicationRoleStates: []*authorizationTypes.ApplicationRoleState{
					{
						Creator:           creatorA,
						Id:                authorizationTypes.BusinessLogicStoreDocumentHash,
						ApplicationRoleID: authorizationTypes.BusinessLogicStoreDocumentHash,
						Description:       "",
						Valid:             true,
						TimeStamp:         "",
					},
				},
			})
			authorizationKeeper.SetBlockchainAccount(ctx, authorizationTypes.BlockchainAccount{
				Creator: creatorA,
				Id:      creatorA,
				BlockchainAccountStates: []*authorizationTypes.BlockchainAccountState{
					{
						Creator:            creatorA,
						Id:                 creatorA,
						AccountID:          creatorA,
						TimeStamp:          "",
						Valid:              true,
						ApplicationRoleIDs: []string{authorizationTypes.BusinessLogicStoreDocumentHash},
					},
				},
			},
			)
			got, err := k.StoreDocumentHash(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.StoreDocumentHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got == nil {
				t.Errorf("msgServer.StoreDocumentHash() error = %v, wantErr %v", got, "id not nil")
				return
			}
		})
	}
}
