// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k msgServer) FetchGetWallet(goCtx context.Context, msg *types.MsgFetchGetWallet) (*tokenwalletTypes.MsgFetchGetWalletResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, errRole := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletQuerySegment)
	if errRole != nil || !check {
		return &tokenwalletTypes.MsgFetchGetWalletResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletQuerySegment)
	}

	res, errWallet := k.walletKeeper.FetchWallet(goCtx, &tokenwalletTypes.MsgFetchGetWallet{
		Creator: msg.Creator,
		Id:      msg.Id,
	})
	if errWallet != nil {
		return &tokenwalletTypes.MsgFetchGetWalletResponse{}, errWallet
	}

	return res, nil
}
