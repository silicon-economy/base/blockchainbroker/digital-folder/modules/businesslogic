// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/google/uuid"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	hashtokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k msgServer) StoreDocumentHash(goCtx context.Context, msg *types.MsgCreateHashToken) (*types.MsgIdResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Keepers
	hashTokenKeeper := k.hashtokenKeeper

	// authorization
	check, errRole := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.BusinessLogicStoreDocumentHash)
	if errRole != nil || !check {
		return &types.MsgIdResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.BusinessLogicStoreDocumentHash)
	}

	documentId := msg.Document
	mapper, exist := k.GetDocumentTokenMapper(ctx, documentId)

	if !exist {
		// Keepers
		tokenwalletKeeper := k.walletKeeper

		segmentResponse, err := tokenwalletKeeper.FetchSegment(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgFetchGetSegment(msg.Creator, msg.SegmentId))

		if err != nil {
			return nil, err
		}

		segment := segmentResponse.Segment
		if segment.WalletId == "" {
			return &types.MsgIdResponse{}, sdkErrors.ErrKeyNotFound
		}

		// Create new TokenRef
		tokenRefId := uuid.New().String()
		tokenId := segment.WalletId + "/" + tokenRefId
		NewTokenCp := types.TokenCopies{
			Creator: msg.Creator,
			Index:   tokenRefId,
			Tokens:  []string{tokenId},
		}
		k.SetTokenCopies(ctx, NewTokenCp)

		k.CreateDocumentTokenMapper(goCtx, &types.MsgCreateDocumentTokenMapper{Creator: msg.Creator, DocumentId: msg.Document, TokenId: tokenId})
		tokenwalletKeeper.CreateTokenRef(goCtx, tokenwalletTypes.NewMsgCreateTokenRef(msg.Creator, tokenRefId, hashtokenTypes.ModuleName, msg.SegmentId))

		msgCreateToken := hashtokenTypes.NewMsgCreateToken(msg.Creator, tokenId, hashtokenTypes.ModuleName, msg.ChangeMessage, msg.SegmentId)
		msgCreateToken.Document = msg.Document
		msgCreateToken.Hash = msg.Hash
		msgCreateToken.HashFunction = msg.HashFunction
		msgCreateToken.Metadata = msg.Metadata
		hashTokenKeeper.CreateToken(goCtx, msgCreateToken)
	} else {
		hashTokenKeeper.UpdateTokenInformation(goCtx, hashtokenTypes.NewMsgUpdateTokenInformation(msg.Creator, mapper.TokenId, msg.ChangeMessage, msg.Document,
			msg.Hash, msg.HashFunction, msg.Metadata))
		k.SetDocumentTokenMapper(ctx, types.DocumentTokenMapper{Creator: msg.Creator, DocumentId: documentId, TokenId: mapper.TokenId})
	}

	return &types.MsgIdResponse{
		Id: documentId,
	}, nil
}
