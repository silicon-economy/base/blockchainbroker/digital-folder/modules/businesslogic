// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k msgServer) UpdateWallet(goCtx context.Context, msg *types.MsgUpdateWallet) (*tokenwalletTypes.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, errRole := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletUpdateWallet)
	if errRole != nil || !check {
		return &tokenwalletTypes.MsgEmptyResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletUpdateWallet)
	}

	res, errWallet := k.walletKeeper.UpdateWallet(goCtx, &tokenwalletTypes.MsgUpdateWallet{
		Creator: msg.Creator,
		Id:      msg.Id,
		Name:    msg.Name,
	})
	if errWallet != nil {
		return &tokenwalletTypes.MsgEmptyResponse{}, errWallet
	}

	return res, nil
}
