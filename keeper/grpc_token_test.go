// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func TestKeeper_TokensBySegmentId(t *testing.T) {
	k, ctx := setupKeeper(t)
	wctx := sdk.WrapSDKContext(ctx)
	srv := NewMsgServerImpl(*k)

	t.Run("Test Tokens by segment Id without existing key", func(t *testing.T) {
		_, err := srv.FetchTokensBySegmentId(wctx, types.NewMsgFetchTokensBySegmentId(creatorA, segmentId1))
		assert.NotNil(t, err)
	})

	tokenWalletKeeper := k.walletKeeper
	res, _ := tokenWalletKeeper.CreateWallet(wctx, tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet"))
	_ = res

	srv.CreateToken(wctx, &types.MsgCreateToken{
		Creator:   creatorA,
		SegmentId: segmentId1,
		ModuleRef: token,
	})

	srv.CreateToken(wctx, &types.MsgCreateToken{
		Creator:   creatorA,
		SegmentId: segmentId1,
		ModuleRef: hashToken,
	})

	t.Run("Test Tokens by segment Id successfully", func(t *testing.T) {
		got, err := srv.FetchTokensBySegmentId(wctx, types.NewMsgFetchTokensBySegmentId(creatorA, segmentId1))
		assert.NotEmpty(t, got)
		assert.Equal(t, 2, len(got.Token)+len(got.HashTokens))
		assert.NoError(t, err)
	})

	t.Run("Test Tokens by wallet Id successfully", func(t *testing.T) {
		got, err := srv.FetchTokensByWalletId(wctx, types.NewMsgFetchTokensByWalletId(creatorA, segmentId1))
		assert.NotEmpty(t, got)
		assert.Equal(t, 2, len(got.Token)+len(got.HashTokens))
		assert.NoError(t, err)
	})
}
