// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func Test_msgServer_MsgFetchGetWalletHistory(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchGetWalletHistory
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *tokenwalletTypes.MsgFetchGetWalletHistoryResponse
		wantErr bool
	}{
		{
			name:   permissionDenied,
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgFetchGetWalletHistory{
				Creator: creatorA,
				Id:      "0",
			}},
			want:    &tokenwalletTypes.MsgFetchGetWalletHistoryResponse{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}
			_, err := k.FetchGetWalletHistory(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.MsgFetchGetWalletHistoryResponse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
