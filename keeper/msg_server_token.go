// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"strings"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"
	"github.com/google/uuid"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	hashtokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
	tokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k msgServer) CreateToken(goCtx context.Context, msg *types.MsgCreateToken) (*types.MsgIdResponse, error) {
	// Keepers
	ctx := sdk.UnwrapSDKContext(goCtx)

	tokenKeeper := k.tokenKeeper
	tokenwalletKeeper := k.walletKeeper
	hashtokenKeeperObj := k.hashtokenKeeper
	segmentResponse, err := tokenwalletKeeper.FetchSegment(goCtx, tokenwalletTypes.NewMsgFetchGetSegment(msg.Creator, msg.SegmentId))

	if err != nil {
		return nil, err
	}
	segment := segmentResponse.Segment
	id := uuid.New().String()
	tokenId := segment.WalletId + "/" + id

	NewTokenCp := types.TokenCopies{
		Creator: msg.Creator,
		Index:   id,
		Tokens:  []string{tokenId},
	}
	k.SetTokenCopies(ctx, NewTokenCp)

	switch msg.ModuleRef {
	case hashtokenTypes.ModuleName:
		_, err1 := hashtokenKeeperObj.CreateToken(goCtx, hashtokenTypes.NewMsgCreateToken(msg.Creator, tokenId, msg.TokenType, msg.ChangeMessage, msg.SegmentId))
		if err1 != nil {
			return nil, err1
		}

		_, err2 := tokenwalletKeeper.CreateTokenRef(goCtx, tokenwalletTypes.NewMsgCreateTokenRef(msg.Creator, id, hashtokenTypes.ModuleName, msg.SegmentId))
		if err2 != nil {
			return nil, err2
		}
	case tokenTypes.ModuleName:
		_, err1 := tokenKeeper.CreateToken(goCtx, tokenTypes.NewMsgCreateToken(msg.Creator, tokenId, msg.TokenType, msg.ChangeMessage, msg.SegmentId))
		if err1 != nil {
			return nil, err1
		}

		_, err2 := tokenwalletKeeper.CreateTokenRef(goCtx, tokenwalletTypes.NewMsgCreateTokenRef(msg.Creator, id, tokenTypes.ModuleName, msg.SegmentId))
		if err2 != nil {
			return nil, err2
		}
	}

	return &types.MsgIdResponse{
		Id: tokenId,
	}, nil
}

func (k msgServer) UpdateToken(goCtx context.Context, msg *types.MsgUpdateToken) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	authorizationKeeper := k.authorizationKeeper
	check, err := authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.BusinessLogicUpdateToken)
	if err != nil || !check {
		return &types.MsgEmptyResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.BusinessLogicUpdateToken)
	}

	// Keepers
	tokenKeeper := k.tokenKeeper
	tokenwalletKeeper := k.walletKeeper
	hashtokenKeeperObj := k.hashtokenKeeper

	tokenCp, _ := k.GetTokenCopies(ctx, msg.TokenRefId)

	segmentResponse, err := tokenwalletKeeper.FetchSegment(goCtx, tokenwalletTypes.NewMsgFetchGetSegment(msg.Creator, msg.SegmentId))

	if err == nil {
		walletId := segmentResponse.Segment.WalletId
		switch msg.ModuleRef {
		case hashtokenTypes.ModuleName:
			if len(tokenCp.Tokens) == 0 {
				hashtokenKeeperObj.UpdateToken(goCtx, hashtokenTypes.NewMsgUpdateToken(msg.Creator, walletId+"/"+msg.TokenRefId, msg.TokenType, msg.ChangeMessage))
			}
			for _, element := range tokenCp.Tokens {
				hashtokenRes, err := hashtokenKeeperObj.FetchToken(goCtx, hashtokenTypes.NewMsgFetchToken(msg.Creator, element))
				hashtoken := hashtokenRes.Token
				_, err = hashtokenKeeperObj.UpdateToken(goCtx, hashtokenTypes.NewMsgUpdateToken(msg.Creator, hashtoken.Id, msg.TokenType, msg.ChangeMessage))
				if err != nil {
					return nil, err
				}
			}
		case tokenTypes.ModuleName:
			if len(tokenCp.Tokens) == 0 {
				tokenKeeper.UpdateToken(goCtx, tokenTypes.NewMsgUpdateToken(msg.Creator, walletId+"/"+msg.TokenRefId, msg.TokenType, msg.ChangeMessage))
			}
			for _, element := range tokenCp.Tokens {
				tokenRes, err := tokenKeeper.FetchToken(goCtx, tokenTypes.NewMsgFetchToken(msg.Creator, element))
				token := tokenRes.Token
				_, err = tokenKeeper.UpdateToken(goCtx, tokenTypes.NewMsgUpdateToken(msg.Creator, token.Id, msg.TokenType, msg.ChangeMessage))
				if err != nil {
					return nil, err
				}
			}
		}

		return &types.MsgEmptyResponse{}, nil
	}
	return nil, err
}

func (k msgServer) ActivateToken(goCtx context.Context, msg *types.MsgActivateToken) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	authorizationKeeper := k.authorizationKeeper
	check, err := authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.BusinessLogicActivateToken)
	if err != nil || !check {
		return &types.MsgEmptyResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.BusinessLogicActivateToken)
	}

	tokenwalletKeeper := k.walletKeeper

	Ids := strings.Split(msg.Id, "/")
	switch msg.ModuleRef {
	case tokenTypes.ModuleName:
		tokenKeeper := k.tokenKeeper
		_, err = tokenKeeper.ActivateToken(goCtx, tokenTypes.NewMsgActivateToken(msg.Creator, msg.Id))
		if err != nil {
			return nil, err
		}
		tokenwalletKeeper.CreateTokenRef(goCtx, tokenwalletTypes.NewMsgCreateTokenRef(msg.Creator, Ids[1], tokenTypes.ModuleName, msg.SegmentId))
	case hashtokenTypes.ModuleName:
		hashtokenKeeperObj := k.hashtokenKeeper
		_, err = hashtokenKeeperObj.ActivateToken(goCtx, hashtokenTypes.NewMsgActivateToken(msg.Creator, msg.Id))
		if err != nil {
			return nil, err
		}
		tokenwalletKeeper.CreateTokenRef(goCtx, tokenwalletTypes.NewMsgCreateTokenRef(msg.Creator, Ids[1], hashtokenTypes.ModuleName, msg.SegmentId))
	}

	return &types.MsgEmptyResponse{}, nil
}

func (k msgServer) DeactivateToken(goCtx context.Context, msg *types.MsgDeactivateToken) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	authorizationKeeper := k.authorizationKeeper
	check, err := authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.BusinessLogicDeactivateToken)
	if err != nil || !check {
		return &types.MsgEmptyResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.BusinessLogicDeactivateToken)
	}

	Ids := strings.Split(msg.Id, "/")
	tokenwalletKeeper := k.walletKeeper

	if !tokenwalletKeeper.HasWallet(ctx, Ids[0]) {
		return nil, sdkErrors.ErrKeyNotFound
	}

	moduleRef, segmentId := k.getModuleRef(ctx, Ids[0], Ids[1], msg.Creator)

	if moduleRef == "" || segmentId == "" {
		return nil, sdkErrors.ErrKeyNotFound
	}

	switch moduleRef {
	case tokenTypes.ModuleName:
		tokenKeeper := k.tokenKeeper
		_, err = tokenKeeper.DeactivateToken(goCtx, tokenTypes.NewMsgDeactivateToken(msg.Creator, msg.Id))
		if err != nil {
			return nil, err
		}
		tokenwalletKeeper.RemoveTokenRefFromSegment(goCtx, tokenwalletTypes.NewMsgRemoveTokenRefFromSegment(msg.Creator, Ids[1], segmentId))
	case hashtokenTypes.ModuleName:
		hashtokenKeeperObj := k.hashtokenKeeper
		_, err = hashtokenKeeperObj.DeactivateToken(goCtx, hashtokenTypes.NewMsgDeactivateToken(msg.Creator, msg.Id))
		if err != nil {
			return nil, sdkErrors.ErrKeyNotFound
		}
		tokenwalletKeeper.RemoveTokenRefFromSegment(goCtx, tokenwalletTypes.NewMsgRemoveTokenRefFromSegment(msg.Creator, Ids[1], segmentId))
	}

	return &types.MsgEmptyResponse{}, nil
}

func (k msgServer) MoveTokenToWallet(goCtx context.Context, msg *types.MsgMoveTokenToWallet) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	authorizationKeeper := k.authorizationKeeper
	check, err := authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.BusinessLogicMoveTokenToWallet)
	if err != nil || !check {
		return &types.MsgEmptyResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.BusinessLogicMoveTokenToWallet)
	}

	// Keepers
	tokenKeeper := k.tokenKeeper
	tokenwalletKeeper := k.walletKeeper
	hashTokenKeeper := k.hashtokenKeeper

	// set old token to false
	segmentRes, err := tokenwalletKeeper.FetchSegment(goCtx, tokenwalletTypes.NewMsgFetchGetSegment(msg.Creator, msg.SourceSegmentId))
	sourceSegment := segmentRes.Segment
	tokenId := sourceSegment.WalletId + "/" + msg.TokenRefId

	// get walletId of the target segment
	segmentRes, err = tokenwalletKeeper.FetchSegment(goCtx, tokenwalletTypes.NewMsgFetchGetSegment(msg.Creator, msg.TargetSegmentId))
	targetSegment := segmentRes.Segment
	targetWalletId := targetSegment.WalletId

	moduleRef, _ := k.getModuleRef(ctx, sourceSegment.WalletId, msg.TokenRefId, msg.Creator)

	if moduleRef == "" {
		return nil, sdkErrors.ErrKeyNotFound
	}

	if moduleRef == "Token" {
		if !tokenKeeper.HasToken(ctx, tokenId) {
			return nil, sdkErrors.ErrKeyNotFound
		}
		sourceTokenRes, _ := tokenKeeper.FetchToken(goCtx, tokenTypes.NewMsgFetchToken(msg.Creator, tokenId))
		sourceToken := sourceTokenRes.Token
		tokenKeeper.SetFalse(ctx, tokenId)
		// create new valid token
		_, createTokenErr := tokenKeeper.CreateToken(goCtx, tokenTypes.NewMsgCreateToken(
			msg.Creator,
			targetWalletId+"/"+msg.TokenRefId,
			sourceToken.TokenType,
			"token moved",
			msg.TargetSegmentId,
		))
		if createTokenErr != nil {
			return &types.MsgEmptyResponse{}, createTokenErr
		}
	} else {
		if !hashTokenKeeper.HasToken(ctx, tokenId) {
			return nil, sdkErrors.ErrKeyNotFound
		}
		sourceTokenRes, _ := hashTokenKeeper.FetchToken(goCtx, hashtokenTypes.NewMsgFetchToken(msg.Creator, tokenId))
		sourceToken := sourceTokenRes.Token
		hashTokenKeeper.SetFalse(ctx, tokenId)
		// create new valid token
		_, createTokenErr := hashTokenKeeper.CreateToken(goCtx, hashtokenTypes.NewMsgCreateToken(
			msg.Creator,
			targetWalletId+"/"+msg.TokenRefId,
			sourceToken.TokenType,
			"hashToken moved",
			msg.TargetSegmentId,
		))
		if createTokenErr != nil {
			return &types.MsgEmptyResponse{}, createTokenErr
		}
	}

	// move the reference in wallet
	tokenwalletKeeper.MoveTokenToWallet(goCtx, tokenwalletTypes.NewMsgMoveTokenToWallet(
		msg.Creator,
		msg.TokenRefId,
		msg.SourceSegmentId,
		msg.TargetSegmentId,
	))

	// update TokenCopies
	tokenCopies, _ := k.GetTokenCopies(ctx, msg.TokenRefId)
	for index, tokenCopy := range tokenCopies.Tokens {
		if tokenCopy == tokenId {
			tokenCopies.Tokens[index] = targetWalletId + "/" + msg.TokenRefId
		}
	}
	return &types.MsgEmptyResponse{}, nil
}

func (k msgServer) CloneToken(goCtx context.Context, msg *types.MsgCloneToken) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	authorizationKeeper := k.authorizationKeeper
	check, err := authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.BusinessLogicCloneToken)
	if err != nil || !check {
		return &types.MsgEmptyResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.BusinessLogicCloneToken)
	}

	tokenwalletKeeper := k.walletKeeper

	wallet, _ := tokenwalletKeeper.FetchWallet(goCtx, tokenwalletTypes.NewMsgFetchGetWallet(msg.Creator, msg.WalletId))
	tokenRefId := strings.Split(msg.TokenId, "/")[1]
	tokenId := msg.WalletId + "/" + tokenRefId

	var segment tokenwalletTypes.Segment
	// create a new token item depending on its moduleRef attribute
	switch msg.ModuleRef {
	case hashtokenTypes.ModuleName:
		hashtokenKeeperObj := k.hashtokenKeeper

		// Get the old hashtoken item
		hashTokenQueryItem, err := hashtokenKeeperObj.FetchToken(goCtx, hashtokenTypes.NewMsgFetchToken(msg.Creator, msg.TokenId))
		if err != nil {
			return nil, err
		}
		hashtoken := hashTokenQueryItem.Token

		if hashtoken.Info == nil {
			hashtoken.Info = &hashtokenTypes.Info{}
		}
		_, err = hashtokenKeeperObj.CreateToken(goCtx, hashtokenTypes.NewMsgCreateToken(msg.Creator, tokenId, hashtoken.TokenType, hashtoken.ChangeMessage, wallet.Wallet.SegmentIds[0]))
		hashtokenKeeperObj.UpdateTokenInformation(goCtx, hashtokenTypes.NewMsgUpdateTokenInformation(msg.Creator, tokenId, hashtoken.ChangeMessage, hashtoken.Info.Document,
			hashtoken.Info.Hash, hashtoken.Info.HashFunction, hashtoken.Info.Metadata))
		if err != nil {
			return &types.MsgEmptyResponse{}, err
		}
	case tokenTypes.ModuleName:
		tokenKeeper := k.tokenKeeper

		tokenRes, _ := tokenKeeper.FetchToken(goCtx, tokenTypes.NewMsgFetchToken(msg.Creator, msg.TokenId))
		token := tokenRes.Token

		_, createTokenErr := tokenKeeper.CreateToken(goCtx, tokenTypes.NewMsgCreateToken(msg.Creator, tokenId, token.TokenType, token.ChangeMessage, wallet.Wallet.SegmentIds[0]))
		if createTokenErr != nil {
			return &types.MsgEmptyResponse{}, createTokenErr
		}
	}

	// Get tokenRef to read moduleRef attribute
	var tokenRef tokenwalletTypes.TokenRef
	for _, element := range segment.TokenRefs {
		if element.Id == tokenRefId {
			tokenRef = *element
		}
	}

	// Add tokenRef item to the new segment
	newSegmentRes, _ := tokenwalletKeeper.FetchSegment(goCtx, tokenwalletTypes.NewMsgFetchGetSegment(msg.Creator, wallet.Wallet.SegmentIds[0]))
	newSegment := newSegmentRes.Segment
	newSegment.TokenRefs = append(newSegment.TokenRefs, &tokenRef)
	tokenwalletKeeper.SetSegment(ctx, *newSegment)

	tokenCopies, _ := k.GetTokenCopies(ctx, tokenRefId)
	if len(tokenCopies.Tokens) > 0 {
		tokenCopies.Tokens = append(tokenCopies.Tokens, tokenId)
		k.SetTokenCopies(ctx, tokenCopies)
	}

	return &types.MsgEmptyResponse{}, nil
}

func (k msgServer) UpdateTokenInformation(goCtx context.Context, msg *types.MsgUpdateTokenInformation) (*types.MsgEmptyResponse, error) {
	_, err := k.tokenKeeper.UpdateTokenInformation(goCtx, tokenTypes.NewMsgUpdateTokenInformation(msg.Creator, msg.TokenId, msg.Data))
	return &types.MsgEmptyResponse{}, err
}

func (k msgServer) getModuleRef(ctx sdk.Context, walletId string, tokenRefId string, creator string) (string, string) {
	walletKeeper := k.walletKeeper
	walletRes, _ := walletKeeper.FetchWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgFetchGetWallet(creator, walletId))
	segmentIds := walletRes.Wallet.SegmentIds

	for _, segmentId := range segmentIds {
		segmentRes, _ := walletKeeper.FetchSegment(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgFetchGetSegment(creator, segmentId))
		segment := segmentRes.Segment

		for _, tokenRef := range segment.TokenRefs {
			if tokenRef.Id == tokenRefId {
				return tokenRef.ModuleRef, segmentId
			}
		}
	}

	return "", ""
}
