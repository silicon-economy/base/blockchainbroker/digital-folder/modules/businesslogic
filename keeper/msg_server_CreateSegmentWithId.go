// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k msgServer) CreateSegmentWithId(goCtx context.Context, msg *types.MsgCreateSegmentWithId) (*tokenwalletTypes.MsgIdResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, errRole := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletCreateSegment)
	if errRole != nil || !check {
		return &tokenwalletTypes.MsgIdResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletCreateSegment)
	}

	res, errSegment := k.walletKeeper.CreateSegmentWithId(goCtx, &tokenwalletTypes.MsgCreateSegmentWithId{
		Creator:  msg.Creator,
		Name:     msg.Name,
		Info:     msg.Info,
		WalletId: msg.WalletId,
		Id:       msg.Id,
	})

	if errSegment != nil {
		return &tokenwalletTypes.MsgIdResponse{}, errSegment
	}

	return res, nil
}
