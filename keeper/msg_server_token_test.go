// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"
	"strings"
	"testing"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_msgServer_createToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgCreateToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   successfulTxTokenModule,
			fields: fields{Keeper: *keeper},
			args: args{
				goCtx: goCtx,
				msg: &types.MsgCreateToken{
					Creator:       creatorA,
					TokenType:     tokenType1,
					ChangeMessage: notChanged,
					SegmentId:     segmentId1,
					ModuleRef:     token,
				}},
			wantErr: false,
		},
		{
			name:   successfulTxHashTokenModule,
			fields: fields{Keeper: *keeper},
			args: args{
				goCtx: goCtx,
				msg: &types.MsgCreateToken{
					Creator:       creatorA,
					TokenType:     tokenType1,
					ChangeMessage: notChanged,
					SegmentId:     segmentId1,
					ModuleRef:     hashToken,
				}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}
			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName))
			got, err := k.CreateToken(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.CreateToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got == nil {
				t.Errorf("msgServer.CreateToken() error = %v, wantErr %v", got, "id not nil")
				return
			}
		})
	}
}

func Test_msgServer_updateToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgUpdateToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:   successfulTxTokenModule,
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgUpdateToken{
				Creator:       creatorA,
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				ModuleRef:     token,
			}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
		{
			name:   successfulTxHashTokenModule,
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgUpdateToken{
				Creator:       creatorA,
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				ModuleRef:     hashToken,
			}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}
			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName))

			id0, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
				Creator:       creatorA,
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				ModuleRef:     token,
			})

			id1, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
				Creator:       creatorA,
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				ModuleRef:     hashToken,
			})

			if tt.args.msg.ModuleRef == token {
				_, err := k.UpdateToken(tt.args.goCtx, &types.MsgUpdateToken{
					Creator:       tt.args.msg.Creator,
					TokenRefId:    strings.Split(id0.Id, "/")[1],
					TokenType:     tt.args.msg.TokenType,
					ChangeMessage: tt.args.msg.ChangeMessage,
					SegmentId:     tt.args.msg.SegmentId,
					ModuleRef:     tt.args.msg.ModuleRef,
				})
				if (err != nil) != tt.wantErr {
					t.Errorf("msgServer.UpdateToken() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			} else {
				_, err := k.UpdateToken(tt.args.goCtx, &types.MsgUpdateToken{
					Creator:       tt.args.msg.Creator,
					TokenRefId:    strings.Split(id1.Id, "/")[1],
					TokenType:     tt.args.msg.TokenType,
					ChangeMessage: tt.args.msg.ChangeMessage,
					SegmentId:     tt.args.msg.SegmentId,
					ModuleRef:     tt.args.msg.ModuleRef,
				})
				if (err != nil) != tt.wantErr {
					t.Errorf("msgServer.UpdateToken() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			}
		})
	}
}

func TestUpdateTokenFailure(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	wCtx := sdk.WrapSDKContext(ctx)
	srv := NewMsgServerImpl(*keeper)

	keeper.walletKeeper.CreateWallet(wCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName))

	performUpdateTokenTests(t, srv, wCtx, hashToken)
	performUpdateTokenTests(t, srv, wCtx, token)
}

func performUpdateTokenTests(t *testing.T, srv types.MsgServer, wCtx context.Context, moduleRef string) {
	tokenId, errorFromCreation := srv.CreateToken(
		wCtx,
		&types.MsgCreateToken{
			Creator:       creatorA,
			TokenType:     tokenType1,
			ChangeMessage: notChanged,
			SegmentId:     segmentId1,
			ModuleRef:     moduleRef,
		},
	)
	require.NoError(t, errorFromCreation)

	t.Run(fmt.Sprintf("Update %s with different creator", moduleRef), func(t *testing.T) {
		response, err := srv.UpdateToken(
			wCtx,
			&types.MsgUpdateToken{
				Creator:       creatorB,
				TokenRefId:    strings.Split(tokenId.Id, "/")[1],
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				ModuleRef:     moduleRef,
			},
		)
		assert.Empty(t, response)
		assert.Error(t, err)
	})

	t.Run(fmt.Sprintf("Update %s with unknown TokenRefId", moduleRef), func(t *testing.T) {
		response, err := srv.UpdateToken(
			wCtx,
			&types.MsgUpdateToken{
				Creator:       creatorA,
				TokenRefId:    "01AB",
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				ModuleRef:     moduleRef,
			},
		)
		assert.Empty(t, response)
		assert.Nil(t, err)
	})
}

func Test_msgServer_activateToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgActivateToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:   successfulTxTokenModule,
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgActivateToken{
				Creator:   creatorA,
				SegmentId: segmentId1,
				ModuleRef: token,
			}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
		{
			name:   successfulTxHashTokenModule,
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgActivateToken{
				Creator:   creatorA,
				SegmentId: segmentId1,
				ModuleRef: hashToken,
			}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}

			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName))

			id0, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
				Creator:       creatorA,
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				ModuleRef:     token,
			})

			id1, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
				Creator:       creatorA,
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				ModuleRef:     hashToken,
			})

			k.DeactivateToken(goCtx, &types.MsgDeactivateToken{
				Creator: creatorA,
				Id:      id0.Id,
			})

			k.DeactivateToken(goCtx, &types.MsgDeactivateToken{
				Creator: creatorA,
				Id:      id1.Id,
			})

			if tt.args.msg.ModuleRef == token {
				_, err := k.ActivateToken(tt.args.goCtx, &types.MsgActivateToken{
					Creator:   tt.args.msg.Creator,
					Id:        id0.Id,
					SegmentId: segmentId1,
					ModuleRef: tt.args.msg.ModuleRef,
				})
				if (err != nil) != tt.wantErr {
					t.Errorf("msgServer.ActivateToken() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			} else {
				_, err := k.ActivateToken(tt.args.goCtx, &types.MsgActivateToken{
					Creator:   tt.args.msg.Creator,
					Id:        id1.Id,
					SegmentId: segmentId1,
					ModuleRef: tt.args.msg.ModuleRef,
				})
				if (err != nil) != tt.wantErr {
					t.Errorf("msgServer.ActivateToken() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			}
		})
	}
}

func TestActivateTokenFailure(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	wCtx := sdk.WrapSDKContext(ctx)
	srv := NewMsgServerImpl(*keeper)

	keeper.walletKeeper.CreateWallet(wCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName))

	performActivateTokenTests(t, srv, wCtx, hashToken)
	performActivateTokenTests(t, srv, wCtx, token)
}

func performActivateTokenTests(t *testing.T, srv types.MsgServer, wCtx context.Context, moduleRef string) {
	tokenId, errorFromCreation := srv.CreateToken(
		wCtx,
		&types.MsgCreateToken{
			Creator:       creatorA,
			TokenType:     tokenType1,
			ChangeMessage: notChanged,
			SegmentId:     segmentId1,
			ModuleRef:     moduleRef,
		},
	)
	require.NoError(t, errorFromCreation)

	t.Run(fmt.Sprintf("Activate %s with different Creator", moduleRef), func(t *testing.T) {
		response, err := srv.ActivateToken(
			wCtx,
			&types.MsgActivateToken{
				Creator:   creatorB,
				Id:        strings.Split(tokenId.Id, "/")[1],
				SegmentId: segmentId1,
				ModuleRef: moduleRef,
			},
		)
		assert.Empty(t, response)
		assert.Error(t, err)
	})

	t.Run(fmt.Sprintf("Activate %s with unknown SegmentId", moduleRef), func(t *testing.T) {
		response, err := srv.ActivateToken(
			wCtx,
			&types.MsgActivateToken{
				Creator:   creatorA,
				Id:        strings.Split(tokenId.Id, "/")[1],
				SegmentId: segmentId2,
				ModuleRef: moduleRef,
			},
		)
		assert.Empty(t, response)
		assert.Error(t, err)
	})

	t.Run(fmt.Sprintf("Activate %s with unknown Id", moduleRef), func(t *testing.T) {
		response, err := srv.ActivateToken(
			wCtx,
			&types.MsgActivateToken{
				Creator:   creatorA,
				Id:        "01AB",
				SegmentId: segmentId1,
				ModuleRef: moduleRef,
			},
		)
		assert.Empty(t, response)
		assert.Error(t, err)
	})
}

func Test_msgServer_deactivateToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgDeactivateToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:   successfulTx,
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgDeactivateToken{
				Creator: creatorA,
			}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}

			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName))

			id0, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
				Creator:       creatorA,
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				ModuleRef:     token,
			})
			id1, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
				Creator:       creatorA,
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				ModuleRef:     hashToken,
			})

			_, tokenErr := k.DeactivateToken(tt.args.goCtx, &types.MsgDeactivateToken{
				Creator: tt.args.msg.Creator,
				Id:      id0.Id,
			})
			if (tokenErr != nil) != tt.wantErr {
				t.Errorf("msgServer.DeactivateToken() error = %v, wantErr %v", tokenErr, tt.wantErr)
				return
			}
			_, hashTokenErr := k.DeactivateToken(tt.args.goCtx, &types.MsgDeactivateToken{
				Creator: tt.args.msg.Creator,
				Id:      id1.Id,
			})
			if (hashTokenErr != nil) != tt.wantErr {
				t.Errorf("msgServer.DeactivateToken() error = %v, wantErr %v", hashTokenErr, tt.wantErr)
				return
			}
		})
	}
}

func TestDeactivateTokenFailure(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	wCtx := sdk.WrapSDKContext(ctx)
	srv := NewMsgServerImpl(*keeper)

	keeper.walletKeeper.CreateWallet(wCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName))

	performDeactivateTokenTests(t, srv, wCtx, hashToken)
	performDeactivateTokenTests(t, srv, wCtx, token)
}

func performDeactivateTokenTests(t *testing.T, srv types.MsgServer, wCtx context.Context, moduleRef string) {
	tokenId, errorFromCreation := srv.CreateToken(
		wCtx,
		&types.MsgCreateToken{
			Creator:       creatorA,
			TokenType:     tokenType1,
			ChangeMessage: notChanged,
			SegmentId:     segmentId1,
			ModuleRef:     moduleRef,
		},
	)
	require.NoError(t, errorFromCreation)

	t.Run(fmt.Sprintf("Deactivate %s with different creator", moduleRef), func(t *testing.T) {
		response, err := srv.DeactivateToken(
			wCtx,
			&types.MsgDeactivateToken{
				Creator: creatorB,
				Id:      strings.Split(tokenId.Id, "/")[1],
			},
		)
		assert.Empty(t, response)
		assert.Error(t, err)
	})

	t.Run(fmt.Sprintf("Deactivate %s with unknown Id", moduleRef), func(t *testing.T) {
		response, err := srv.DeactivateToken(
			wCtx,
			&types.MsgDeactivateToken{
				Creator: creatorA,
				Id:      "01AB",
			},
		)
		assert.Empty(t, response)
		assert.Error(t, err)
	})
}

func Test_msgServer_moveTokenToWallet(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgMoveTokenToWallet
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:   successfulTx,
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgMoveTokenToWallet{
				Creator:         creatorA,
				SourceSegmentId: segmentId1,
				TargetSegmentId: segmentId2,
			}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}

			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet1"))
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "newWallet2"))

			id0, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
				Creator:       creatorA,
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId1,
				ModuleRef:     token,
			})

			_, tokenErr := k.MoveTokenToWallet(tt.args.goCtx, &types.MsgMoveTokenToWallet{
				Creator:         tt.args.msg.Creator,
				TokenRefId:      strings.Split(id0.Id, "/")[1],
				SourceSegmentId: tt.args.msg.SourceSegmentId,
				TargetSegmentId: tt.args.msg.TargetSegmentId,
			})
			if (tokenErr != nil) != tt.wantErr {
				t.Errorf("msgServer.MoveTokenToWallet() error = %v, wantErr %v", tokenErr, tt.wantErr)
				return
			}
		})
	}
}

func TestMoveTokenToWalletFailure(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	wCtx := sdk.WrapSDKContext(ctx)
	srv := NewMsgServerImpl(*keeper)

	keeper.walletKeeper.CreateWallet(wCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName))

	performMoveTokenToWalletTests(t, srv, wCtx, hashToken)
	performMoveTokenToWalletTests(t, srv, wCtx, token)
}

func performMoveTokenToWalletTests(t *testing.T, srv types.MsgServer, wCtx context.Context, moduleRef string) {
	tokenId, errorFromCreation := srv.CreateToken(
		wCtx,
		&types.MsgCreateToken{
			Creator:       creatorA,
			TokenType:     tokenType1,
			ChangeMessage: notChanged,
			SegmentId:     segmentId1,
			ModuleRef:     moduleRef,
		},
	)
	require.NoError(t, errorFromCreation)

	t.Run(fmt.Sprintf("Move %s to wallet with different Creator", moduleRef), func(t *testing.T) {
		response, err := srv.MoveTokenToWallet(
			wCtx,
			&types.MsgMoveTokenToWallet{
				Creator:         creatorB,
				TokenRefId:      strings.Split(tokenId.Id, "/")[1],
				SourceSegmentId: segmentId1,
				TargetSegmentId: segmentId2,
			},
		)
		assert.Empty(t, response)
		assert.Error(t, err)
	})
}

func Test_msgServer_cloneToken(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgCloneToken
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgEmptyResponse
		wantErr bool
	}{
		{
			name:   successfulTxTokenModule,
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgCloneToken{
				Creator:   creatorA,
				WalletId:  walletId,
				ModuleRef: token,
			}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
		{
			name:   successfulTxHashTokenModule,
			fields: fields{Keeper: *keeper},
			args: args{goCtx: goCtx, msg: &types.MsgCloneToken{
				Creator:   creatorA,
				WalletId:  walletId,
				ModuleRef: hashToken,
			}},
			want:    &types.MsgEmptyResponse{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}

			tokenwalletKeeper := k.walletKeeper
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName))
			tokenwalletKeeper.CreateWallet(sdk.WrapSDKContext(ctx), tokenwalletTypes.NewMsgCreateWallet(creatorA, "Example Wallet 2"))

			id0, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
				Creator:       creatorA,
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId2,
				ModuleRef:     token,
			})
			id1, _ := k.CreateToken(goCtx, &types.MsgCreateToken{
				Creator:       creatorA,
				TokenType:     tokenType1,
				ChangeMessage: notChanged,
				SegmentId:     segmentId2,
				ModuleRef:     hashToken,
			})

			if tt.args.msg.ModuleRef == token {
				_, err := k.CloneToken(tt.args.goCtx, &types.MsgCloneToken{
					Creator:   tt.args.msg.Creator,
					TokenId:   id0.Id,
					WalletId:  tt.args.msg.WalletId,
					ModuleRef: tt.args.msg.ModuleRef,
				})
				if (err != nil) != tt.wantErr {
					t.Errorf("msgServer.CloneToken() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			} else {
				_, err := k.CloneToken(tt.args.goCtx, &types.MsgCloneToken{
					Creator:   tt.args.msg.Creator,
					TokenId:   id1.Id,
					WalletId:  tt.args.msg.WalletId,
					ModuleRef: tt.args.msg.ModuleRef,
				})
				if (err != nil) != tt.wantErr {
					t.Errorf("msgServer.CloneToken() error = %v, wantErr %v", err, tt.wantErr)
					return
				}
			}
		})
	}
}

func TestCloneTokenFailure(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	wCtx := sdk.WrapSDKContext(ctx)
	srv := NewMsgServerImpl(*keeper)

	keeper.walletKeeper.CreateWallet(wCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, walletName))
	targetWalletId, _ := keeper.walletKeeper.CreateWallet(wCtx, tokenwalletTypes.NewMsgCreateWallet(creatorA, "New Wallet"))

	performCloneTokenTests(t, srv, wCtx, hashToken, targetWalletId.Id)
	performCloneTokenTests(t, srv, wCtx, token, targetWalletId.Id)
}

func performCloneTokenTests(t *testing.T, srv types.MsgServer, wCtx context.Context, moduleRef string, targetWalletId string) {
	tokenId, errorFromCreation := srv.CreateToken(
		wCtx,
		&types.MsgCreateToken{
			Creator:       creatorA,
			TokenType:     tokenType1,
			ChangeMessage: notChanged,
			SegmentId:     segmentId1,
			ModuleRef:     moduleRef,
		},
	)
	require.NoError(t, errorFromCreation)

	t.Run(fmt.Sprintf("Clone %s with different Creator", moduleRef), func(t *testing.T) {
		response, err := srv.CloneToken(
			wCtx,
			&types.MsgCloneToken{
				Creator:   creatorB,
				TokenId:   strings.Split(tokenId.Id, "/")[1],
				WalletId:  targetWalletId,
				ModuleRef: moduleRef,
			},
		)
		assert.Empty(t, response)
		assert.Error(t, err)
	})
}
