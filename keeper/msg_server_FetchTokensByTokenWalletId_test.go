// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
)

func Test_msgServer_FetchTokensByWalletId(t *testing.T) {
	keeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)
	type fields struct {
		Keeper Keeper
	}
	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchTokensByWalletId
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.QueryAllTokenResponse
		wantErr bool
	}{
		{
			name:    keyNotFound,
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchTokensByWalletId{Creator: creatorA}},
			want:    &types.QueryAllTokenResponse{},
			wantErr: true,
		},
		{
			name:    permissionDenied,
			fields:  fields{Keeper: *keeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchTokensByWalletId{Creator: creatorB}},
			want:    &types.QueryAllTokenResponse{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			k := msgServer{
				Keeper: tt.fields.Keeper,
			}
			authorizationKeeper := k.authorizationKeeper
			authorizationKeeper.SetApplicationRole(ctx, authorizationTypes.ApplicationRole{
				Creator: creatorA,
				Id:      authorizationTypes.BusinessLogicGetTokensByWalletId,
				ApplicationRoleStates: []*authorizationTypes.ApplicationRoleState{
					{
						Creator:           creatorA,
						Id:                authorizationTypes.BusinessLogicGetTokensByWalletId,
						ApplicationRoleID: authorizationTypes.BusinessLogicGetTokensByWalletId,
						Description:       "",
						Valid:             true,
						TimeStamp:         "",
					},
				},
			})
			authorizationKeeper.SetBlockchainAccount(ctx, authorizationTypes.BlockchainAccount{
				Creator: creatorA,
				Id:      creatorA,
				BlockchainAccountStates: []*authorizationTypes.BlockchainAccountState{
					{
						Creator:            creatorA,
						Id:                 creatorA,
						AccountID:          creatorA,
						TimeStamp:          "",
						Valid:              true,
						ApplicationRoleIDs: []string{authorizationTypes.BusinessLogicGetTokensByWalletId},
					},
				},
			},
			)
			_, err := k.FetchTokensByWalletId(tt.args.goCtx, tt.args.msg)
			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchTokensByWalletId() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
