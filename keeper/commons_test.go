// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"testing"
	"time"

	"github.com/cosmos/cosmos-sdk/codec"
	codecTypes "github.com/cosmos/cosmos-sdk/codec/types"
	"github.com/cosmos/cosmos-sdk/store"
	storeTypes "github.com/cosmos/cosmos-sdk/store/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"github.com/tendermint/tendermint/libs/log"
	tendermintTypes "github.com/tendermint/tendermint/proto/tendermint/types"
	tendermintTmDb "github.com/tendermint/tm-db"

	authorizationKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/keeper"
	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	hashtokenKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/keeper"
	hashtokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/hashtoken/types"
	tokenKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/keeper"
	tokenTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
	tokenwalletKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/keeper"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

// all tests need to be in the same package as the production code, because otherwise we get an import cycle error
// therefore we need to define the constants from the test_helpers in here
const (
	creatorA = "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462y"
	creatorB = "cosmos16eud9mdv40s8mvmgf9w9dk0v29y420hq3z462x"

	tokenType1 = "Example Token Type 1"
	tokenType2 = "Example Token Type 2"
	token      = "Token"
	hashToken  = "HashToken"

	walletId   = "0"
	walletName = "Example Wallet Name"

	segmentId1 = "0"
	segmentId2 = "1"

	changed      = "changed"
	notChanged   = "not changed"
	document     = "document"
	hash         = "hash"
	hashFunction = "sha256"
	metadata     = "metadata"

	successfulTx                = "Successful Tx"
	successfulTxTokenModule     = "Successful Tx Token Module"
	successfulTxHashTokenModule = "Successful Tx HashToken Module"
	permissionDenied            = "Tx Permission denied"
	keyNotFound                 = "Key not found"
)

func setupKeeper(t testing.TB) (*Keeper, sdk.Context) {
	storeKey := sdk.NewKVStoreKey(types.StoreKey)
	walletStoreKey := sdk.NewKVStoreKey(tokenwalletTypes.StoreKey)
	tokenStoreKey := sdk.NewKVStoreKey(tokenTypes.StoreKey)
	hashTokenStoreKey := sdk.NewKVStoreKey(hashtokenTypes.StoreKey)
	authorizationKey := sdk.NewKVStoreKey(authorizationTypes.StoreKey)
	memStoreKey := storeTypes.NewMemoryStoreKey(types.MemStoreKey)

	db := tendermintTmDb.NewMemDB()
	stateStore := store.NewCommitMultiStore(db)
	stateStore.MountStoreWithDB(storeKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(walletStoreKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(tokenStoreKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(hashTokenStoreKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(authorizationKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(memStoreKey, sdk.StoreTypeMemory, nil)
	require.NoError(t, stateStore.LoadLatestVersion())

	registry := codecTypes.NewInterfaceRegistry()

	newTokenWalletKeeper := *tokenwalletKeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		walletStoreKey,
		memStoreKey,
	)

	newHashTokenKeeper := *hashtokenKeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		hashTokenStoreKey,
		memStoreKey,
	)

	newAuthorizationKeeper := *authorizationKeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		authorizationKey,
		memStoreKey,
	)

	newTokenKeeper := *tokenKeeper.NewKeeper(
		codec.NewProtoCodec(registry),
		tokenStoreKey,
		memStoreKey,
	)

	keeper := NewKeeper(
		codec.NewProtoCodec(registry),
		storeKey,
		newTokenWalletKeeper,
		newTokenKeeper,
		newHashTokenKeeper,
		newAuthorizationKeeper,
		memStoreKey,
	)

	roleIds := []string{
		authorizationTypes.BusinessLogicActivateToken,
		authorizationTypes.BusinessLogicCloneToken,
		authorizationTypes.BusinessLogicCreateToken,
		authorizationTypes.BusinessLogicDeactivateToken,
		authorizationTypes.BusinessLogicGetDocumentHash,
		authorizationTypes.BusinessLogicGetTokensBySegmentId,
		authorizationTypes.BusinessLogicGetTokensByWalletId,
		authorizationTypes.BusinessLogicMoveTokenToWallet,
		authorizationTypes.BusinessLogicSetTokenValidStatus,
		authorizationTypes.BusinessLogicStoreDocumentHash,
		authorizationTypes.BusinessLogicUpdateToken,
		authorizationTypes.BusinessLogicGetTokensBySegmentId,
		authorizationTypes.HashTokenActivateToken,
		authorizationTypes.HashTokenCreateToken,
		authorizationTypes.HashTokenDeactivateToken,
		authorizationTypes.HashTokenUpdateToken,
		authorizationTypes.HashTokenUpdateTokenInformation,
	}
	ctx := sdk.NewContext(stateStore, tendermintTypes.Header{}, false, log.NewNopLogger())

	for _, v := range roleIds {
		newAuthorizationKeeper.AppendApplicationRole(ctx, "", v, "")
	}

	newAuthorizationKeeper.SetBlockchainAccount(ctx, authorizationTypes.BlockchainAccount{
		Creator: creatorA,
		Id:      creatorA,
		BlockchainAccountStates: []*authorizationTypes.BlockchainAccountState{{
			Creator:            creatorA,
			Id:                 uuid.New().String(),
			AccountID:          creatorA,
			TimeStamp:          time.Now().UTC().Format(time.RFC3339),
			ApplicationRoleIDs: roleIds,
			Valid:              true,
		}},
	})

	newAuthorizationKeeper.SetConfiguration(ctx, authorizationTypes.Configuration{Creator: "", Id: 0, PermissionCheck: true})

	return keeper, ctx
}
