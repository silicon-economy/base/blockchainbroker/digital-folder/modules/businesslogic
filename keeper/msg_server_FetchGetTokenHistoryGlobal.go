// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/businesslogic/types"
	tokenwalletTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/tokenwallet/types"
)

func (k msgServer) FetchGetTokenHistoryGlobal(goCtx context.Context, msg *types.MsgFetchGetTokenHistoryGlobal) (*tokenwalletTypes.MsgFetchGetTokenHistoryGlobalResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Permission check
	check, errRole := k.authorizationKeeper.HasRole(ctx, msg.Creator, authorizationTypes.WalletQueryTokenHistoryGlobal)
	if errRole != nil || !check {
		return &tokenwalletTypes.MsgFetchGetTokenHistoryGlobalResponse{}, sdkErrors.Wrap(authorizationTypes.ErrNoPermission, authorizationTypes.WalletQueryTokenHistoryGlobal)
	}

	res, errTokenHistory := k.walletKeeper.FetchTokenHistoryGlobal(goCtx, &tokenwalletTypes.MsgFetchGetTokenHistoryGlobal{
		Creator: msg.Creator,
		Id:      msg.Id,
	})
	if errTokenHistory != nil {
		return &tokenwalletTypes.MsgFetchGetTokenHistoryGlobalResponse{}, errTokenHistory
	}

	return res, nil
}
